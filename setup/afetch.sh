#!/bin/sh

git clone https://github.com/13-CF/afetch
cd afetch

# Display colors
sed -i '13s/false/true/' src/config.h
make

mv -fv afetch ~/.local/bin/afetch
cd ..
