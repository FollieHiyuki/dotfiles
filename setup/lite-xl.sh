#!/bin/sh

if [ ! -d "$HOME/.local/share/lite-xl" ]; then
  mkdir -pv ~/.local/share/lite-xl
  git clone https://github.com/franko/console ~/.local/share/lite-xl/console
  git clone https://github.com/liquidev/lintplus ~/.local/share/lite-xl/lintplus
  git clone https://github.com/drmargarido/linters ~/.local/share/lite-xl/linters
  git clone https://github.com/franko/lite-plugins ~/.local/share/lite-xl/lite-plugins
  # git clone https://github.com/a327ex/lite-plugins ~/.local/share/lite-xl/lite-plugins-2
fi
