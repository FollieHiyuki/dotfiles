#!/bin/sh

git clone https://github.com/soreau/wayland-logout.git
cd wayland-logout

echo "Build wayland-logout"
meson build --prefix $HOME/.local
ninja -C build install

cd ..
