#!/bin/sh

# Need wlroots-devel to build
git clone https://github.com/ifreund/river.git
cd river

echo "Initialize submodules"
git submodule update --init --recursive

echo "Install river"
zig build -Drelease-safe -Dxwayland --prefix $HOME/.local install
rm -rv ~/.local/etc

cd ..
