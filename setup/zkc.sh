#!/bin/sh

# You need sqlite-devel
git clone https://git.sr.ht/~fancycade/zkc
cd zkc
meson build --prefix=$HOME/.local
ninja -C build install

cd ..
