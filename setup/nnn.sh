#!/bin/sh

# Needs musl-fts, ncurses-devel

git clone https://github.com/FollieHiyuki/nnn.git
cd nnn

LDLIBS=-lfts make O_NERD=1 O_NOBATCH=1 O_GITSTATUS=1 PREFIX=$HOME/.local install

cd ..
