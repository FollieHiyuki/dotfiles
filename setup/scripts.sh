#!/bin/sh

curl -fL "https://github.com/sdushantha/tmpsms/raw/master/tmpsms" -o $HOME/.local/bin/tmpsms
chmod 755 $HOME/.local/bin/tmpsms

curl -fL "https://github.com/sdushantha/tmpmail/raw/master/tmpmail" -o $HOME/.local/bin/tmpmail
chmod 755 $HOME/.local/bin/tmpmail

curl -fL "https://github.com/sdushantha/farge/raw/master/farge" -o $HOME/.local/bin/farge
chmod 755 $HOME/.local/bin/farge

curl -fL "https://raw.githubusercontent.com/Bugswriter/tuxi/main/tuxi" -o $HOME/.local/bin/tuxi
chmod 755 $HOME/.local/bin/tuxi

curl -fL "https://github.com/RaitaroH/adl/raw/master/adl" -o $HOME/.local/bin/fzf/adl
chmod 755 $HOME/.local/bin/fzf/adl

curl -fL "https://github.com/pystardust/ytfzf/raw/master/ytfzf" -o $HOME/.local/bin/fzf/ytfzf
chmod 755 $HOME/.local/bin/fzf/ytfzf

curl -fL "https://github.com/pystardust/ani-cli/raw/master/ani-cli" -o $HOME/.local/bin/ani-cli
chmod 755 $HOME/.local/bin/ani-cli

curl -fL "https://github.com/dilshod/xlsx2csv/raw/master/xlsx2csv.py" -o $HOME/.local/bin/xlsx2csv.py
chmod 755 $HOME/.local/bin/xlsx2csv.py

curl -fL "https://cht.sh/:cht.sh" -o $HOME/.local/bin/cht.sh
chmod 755 $HOME/.local/bin/cht.sh
echo "Install standalone cht.sh (y/n): " | tr -d '\n'
read -r answer
if [ "${answer}" = "y" ]; then
  $HOME/.local/bin/cht.sh --standalone-install $HOME/.local/share/cheat.sh
fi
