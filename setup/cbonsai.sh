#!/bin/sh

# You need ncurses-devel

git clone https://gitlab.com/jallbrit/cbonsai
cd cbonsai

make PREFIX=$HOME/.local install

cd ..
