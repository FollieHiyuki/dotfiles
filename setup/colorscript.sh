#!/bin/sh

git clone https://gitlab.com/dwt1/shell-color-scripts.git

cp -rfv ./shell-color-scripts/colorscripts ~/.local/bin/
cp -fv ./shell-color-scripts/colorscript.sh ~/.local/bin/colorscript
chmod 755 ~/.local/bin/colorscript
sed -i '5s|/opt/shell-color-scripts|$HOME/.local/bin|' ~/.local/bin/colorscript
