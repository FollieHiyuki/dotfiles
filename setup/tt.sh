#!/bin/sh

# You need ncurses-devel
git clone https://github.com/runrin/tt.git
cd tt

make
chmod 755 tt
mv -fv tt ~/.local/bin/tt

cd ..
