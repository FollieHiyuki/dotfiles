#!/bin/sh

# You need mpv-devel, meson, ninja, pkg-config

git clone https://github.com/GhostNaN/mpvpaper

cd mpvpaper
meson build --prefix=$HOME/.local
ninja -C build install

cd ..

# Control the wallpaper with mpv input-ipc-server:
# mpvpaper -f -o "input-ipc-server=/tmp/mpv-socket" DP-1 /path/to/video
# echo 'cycle pause' | socat - /tmp/mpv-socket

# Simply set the wallpaper
# mpvpaper -f -o "--no-audio --loop-file" DP-1 /path/to/video
