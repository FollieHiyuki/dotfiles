#!/bin/sh

curl -fLo xwinwrap.c https://github.com/mmhobi7/xwinwrap/raw/master/xwinwrap.c
cc -g -O2 -Wall -Wextra -lX11 -lXext -lXrender -o xwinwrap xwinwrap.c

# Install
chmod 755 xwinwrap
mv -fv xwinwrap ~/.local/bin/xwinwrap
