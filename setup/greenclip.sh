#!/bin/sh

echo "Version of greenclip: " | tr -d '\n' # 4.2
read -r version

curl -fLo greenclip https://github.com/erebe/greenclip/releases/download/${version}/greenclip
chmod 755 greenclip
mv -fv greenclip ~/.local/bin/greenclip
