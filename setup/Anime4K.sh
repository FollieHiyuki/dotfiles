#!/bin/sh

echo "Number of make jobs: " | tr -d '\n' # 4
read -r mj

# ------------------ Anime4KCPP -------------------------- #
# Need cmake, libopencv4-devel, ocl-icd-devel, libgomp-devel
git clone https://github.com/TianZerl/Anime4KCPP.git
cd Anime4KCPP
mkdir build && cd build
cmake ..
make -j${mj}
cp -f bin/Anime4KCPP_CLI ~/.local/bin/
cd ../..

# ----------------- Anime4K shaders ---------------------- #
echo "Version of Anime4K shaders: " | tr -d '\n' # 4.0.1
read -r version
if [ -d "$HOME/.config/mpv/shaders" ]; then
    rm -rf ~/.config/mpv/shaders
fi
curl -fLo Anime4K.zip https://github.com/bloc97/Anime4K/releases/download/${version}/Anime4K_v4.0.zip
unzip Anime4K.zip -d ~/.config/mpv/shaders && rm Anime4K.zip

# ------------------- PyWrapper -------------------------- #
# Need pymkv, pymediainfo, simple-term-menu (use pip inside a virtualenv)
git clone https://github.com/ThoughtfulDev/Anime4K.git ~/.local/bin/Anime4K-PyWrapper
cp -rfv shaders ~/.local/bin/Anime4K-PyWrapper/

# ----------------- waifu2x-converter-cpp ---------------- #
# Need opencv4 (binary), ocl-icd-devel, cmake (and cuda if using Nvidia)
git clone https://github.com/DeadSix27/waifu2x-converter-cpp
cd waifu2x-converter-cpp
mkdir build && cd build
cmake ..
make -j${mj}
cp -f waifu2x-converter-cpp ~/.local/bin/
cd ../..

# ------------------ Video2X ----------------------------- #
# Read the `requirements.txt`.
# Change the config file video2x.yaml (we don't use Windows)
#git clone https://github.com/k4yt3x/video2x.git
#cp -rfv video2x/src ~/.local/bin/video2x

# -- Dandere2x, waifu2x, waifu2x-ncnn-vulkan -- #
