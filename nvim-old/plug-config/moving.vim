" Consistent quickscope colors in colorschemes
augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
augroup END

" Sneak
let g:sneak#label = 1
" QuickScope
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
let g:qs_enable=1
let g:qs_buftype_blacklist = ['terminal', 'nofile']
let g:qs_lazy_highlight = 1
