" Basics
let g:trans_join_lines = 1
let g:trans_win_height = 15

" Directions
" let g:trans_interactive_full_list = 1
let g:trans_default_direction = ":ja"
let g:trans_directions_list = [
  \ ['', 'en'],
  \ ['', 'ja'],
  \ ['', 'de'],
  \ ['', 'ru'],
  \ ['', 'fr'],
  \ ['', 'nl'],
  \ ['', 'la'],
  \ ['', 'es'],
  \ ['', 'zh-CN'],
  \ ['', 'zh-TW'],
  \ ]

nnoremap <silent> <leader>jt :Trans<CR>
vnoremap <silent> <leader>jt :Trans<CR>
nnoremap <silent> <leader>jd :TransSelectDirection<CR>
vnoremap <silent> <leader>jd :TransSelectDirection<CR>
nnoremap <silent> <leader>jr cw<C-R>=system('trans -brief -no-ansi', getreg(""))[:-2]<CR>
vnoremap <silent> <leader>jr c<C-R>=system('trans -brief -no-ansi', getreg(""))[:-2]<CR>
nnoremap <silent> <leader>jc cw<C-R>=system('trans -brief -no-ansi :', getreg(""))[:-2]<S-Left><S-Left><Right>
vnoremap <silent> <leader>jc c<C-R>=system('trans -brief -no-ansi :', getreg(""))[:-2]<S-Left><S-Left><Right>
