if g:colors_name == 'onedark'
  let g:spaceline_colorscheme = 'one'
elseif g:colors_name == 'nord'
  let g:spaceline_colorscheme = 'nord'
endif
let g:spaceline_seperate_style= 'arrow-fade'
let g:spaceline_custom_vim_status = {"n": "NORMAL","V":"V-LINE","v":"VISUAL","\<C-v>": "V-BLOCK","i":"INSERT","R":"REPLACE","r":"REPLACE","s":"S","t":"T","c":"COMMAND","!":"SE"}
let g:spaceline_diagnostic_tool = 'ale'
let g:spaceline_diagnostic_errorsign = '●'
let g:spaceline_diagnostic_warnsign = '●'
let g:spaceline_diagnostic_oksign = ''
let g:spaceline_git_branch_icon = ' '
let g:spaceline_custom_diff_icon = ['+','~','-']
let g:spaceline_diff_tool = 'git-gutter'
let g:spaceline_function_icon = ' '
let g:spaceline_scroll_chars = [
  \  ' ', '▁', '▂', '▃', '▄', '▅', '▆', '▇', '█'
  \  ]
