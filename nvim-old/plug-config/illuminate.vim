" augroup illuminate_augroup
"     autocmd!
"     autocmd VimEnter * hi link illuminatedWord Visual
" augroup END

let g:Illuminate_ftblacklist = ['javascript', 'jsx', 'html', 'nerdtree']
