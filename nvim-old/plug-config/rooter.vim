let g:rooter_patterns = ['.git', 'Makefile', '_darcs', '.hg', '.bzr', '.svn', '*.pro', '.projectile', 'Dockerfile']
let g:rooter_change_directory_for_non_project_files = 'current'
let g:rooter_silent_chdir = 1
let g:rooter_resolve_links = 1
let g:rooter_cd_cmd='lcd'
