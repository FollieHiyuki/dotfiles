" Persistent undo history
if has("persistent_undo")
    set undodir=$HOME/.local/share/nvim/undodir
    set undofile
endif

let g:undotree_WindowLayout = 4
let g:undotree_ShortIndicators = 1
let g:undotree_SplitWidth = 24
let g:undotree_DiffpanelHeight = 10
let g:undotree_DiffAutoOpen = 1
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_RelativeTimestamp = 1
let g:undotree_HelpLine = 1
let g:undotree_HighlightChangedText = 1
let g:undotree_HighlightChangedWithSign = 0
