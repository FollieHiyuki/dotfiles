let g:vimwiki_global_ext = 0
let g:vimwiki_filetypes = ['markdown']
let g:vimwiki_list = [{'path': '$HOME/.local/share/nvim/wiki/',
    \ 'syntax': 'markdown', 'ext': '.md'}]
