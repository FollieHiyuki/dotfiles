let g:buffet_always_show_tabline = 1
let g:buffet_show_index = 1
let g:buffet_max_plug = 10
let g:buffet_powerline_separators = 1
let g:buffet_use_devicons = 1
let g:buffet_tab_icon = "\uf9f0"
let g:buffet_left_trunc_icon = "\uf0a8"
let g:buffet_right_trunc_icon = "\uf0a9"
let g:buffet_modified_icon = '+'
let g:buffet_new_buffer_name = '*'

if g:colors_name == "nord"
  function! g:BuffetSetCustomColors()
    hi! BuffetCurrentBuffer cterm=NONE ctermbg=6 ctermfg=8 guibg=#88c0d0 guifg=#2e3440
    hi! BuffetActiveBuffer cterm=NONE ctermbg=8 ctermfg=6 guibg=#3b4252 guifg=#88c0d0
    hi! BuffetBuffer cterm=NONE ctermbg=8 ctermfg=7 guibg=#3b4252 guifg=#d8dee9
    hi! BuffetModCurrentBuffer cterm=NONE ctermbg=2 ctermfg=8 guibg=#a3be8c guifg=#2e3440
    hi! BuffetModActiveBuffer cterm=NONE ctermbg=8 ctermfg=2 guibg=#3b4252 guifg=#a3be8c
    hi! BuffetTrunc cterm=NONE ctermbg=8 ctermfg=7 guibg=#2e3440 guifg=#d8dee9
    hi! BuffetTab cterm=NONE ctermbg=4 ctermfg=8 guibg=#81a1c1 guifg=#2e3440

    hi! link BuffetModBuffer BuffetBuffer
    hi! link BuffetLeftTrunc BuffetTrunc
    hi! link BuffetRightTrunc BuffetTrunc
    hi! link BuffetEnd BuffetBuffer
  endfunction
elseif g:colors_name == "onedark"
  function! g:BuffetSetCustomColors()
    hi! BuffetCurrentBuffer cterm=NONE ctermbg=4 ctermfg=8 guibg=#61afef guifg=#282c34
    hi! BuffetActiveBuffer cterm=NONE ctermbg=8 ctermfg=4 guibg=#3e4452 guifg=#61afef
    hi! BuffetBuffer cterm=NONE ctermbg=8 ctermfg=7 guibg=#3e4452 guifg=#abb2bf
    hi! BuffetModCurrentBuffer cterm=NONE ctermbg=2 ctermfg=8 guibg=#98c379 guifg=#282c34
    hi! BuffetModActiveBuffer cterm=NONE ctermbg=8 ctermfg=2 guibg=#3e4452 guifg=#98c379
    hi! BuffetTrunc cterm=NONE ctermbg=8 ctermfg=7 guibg=#282c34 guifg=#abb2bf
    hi! BuffetTab cterm=NONE ctermbg=5 ctermfg=8 guibg=#c678dd guifg=#282c34

    hi! link BuffetModBuffer BuffetBuffer
    hi! link BuffetLeftTrunc BuffetTrunc
    hi! link BuffetRightTrunc BuffetTrunc
    hi! link BuffetEnd BuffetBuffer
  endfunction
endif
