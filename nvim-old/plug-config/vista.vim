let g:vista_disable_statusline = 0
let g:vista_default_executive = 'coc'
let g:vista_enable_markdown_extension = 1
let g:vista#renderer#enable_icon = 1
let g:vista_icon_indent = ["▸ ", ""]
let g:vista_fzf_preview = ['right:50%']
let g:vista_keep_fzf_colors = 1
let g:vista_sidebar_width = 30
