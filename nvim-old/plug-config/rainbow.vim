" Rainbow parentheses
let g:rainbow_active = 1
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]
"'guifgs': ['#e06c75', '#98c379', '#e5c07b', '#61afef', '#c678dd', '#56b6c2'],
let g:rainbow_conf = { 
\  'guifgs': ['#bf616a', '#a3be8c', '#ebcb8b', '#81a1c1', '#b48ead', '#88c0d0'],
\  'ctermfgs': ['lightred', 'lightgreen', 'lightyellow', 'lightblue', 'lightmagenta', 'lightcyan'],
\  'guis': ['bold'],
\  'separately': {
\    '*': {},
\    'markdown': {
\      'parentheses_options': 'containedin=markdownCode contained',
\    },
\    'haskell': {
\      'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/\v\{\ze[^-]/ end=/}/ fold'],
\    },
\    'vim': {
\      'parentheses_options': 'containedin=vimFuncBody',
\    },
\    'perl': {
\      'syn_name_prefix': 'perlBlockFoldRainbow',
\    },
\    'stylus': {
\      'parentheses': ['start=/{/ end=/}/ fold contains=@colorableGroup'],
\    },
\    'css': 0,
\  }
\}

" Rainbow csv
let g:disable_rainbow_key_mappings = 1
