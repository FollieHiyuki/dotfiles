set lazyredraw " improve scrolling speed a bit

let g:far#enable_undo=1
let g:far#source='rgnvim'
