autocmd FileType dashboard set showtabline=0 | autocmd WinLeave <buffer> set showtabline=2

let g:dashboard_custom_header = [
  \ "<-. (`-')_  (`-')  _                 (`-')  _     <-. (`-')  ",
  \ "   \\( OO) ) ( OO).-/     .->        _(OO ) (_)       \\(OO )_ ",
  \ ",--./ ,--/ (,------.(`-')----. ,--.(_/,-.\\ ,-(`-'),--./  ,-.)",
  \ "|   \\ |  |  |  .---'( OO).-.  '\\   \\ / (_/ | ( OO)|   `.'   |",
  \ "|  . '|  |)(|  '--. ( _) | |  | \\   /   /  |  |  )|  |'.'|  |",
  \ "|  |\\    |  |  .--'  \\|  |)|  |_ \\     /_)(|  |_/ |  |   |  |",
  \ "|  | \\   |  |  `---.  '  '-'  '\\-'\\   /    |  |'->|  |   |  |",
  \ "`--'  `--'  `------'   `-----'     `-'     `--'   `--'   `--'",
  \ ]
let g:dashboard_default_executive ='fzf'
let g:dashboard_fzf_float = 1
let g:dashboard_fzf_engine = 'rg'
let g:dashboard_session_directory = '$HOME/.local/share/nvim/sessions'

" Icons
let g:dashboard_custom_shortcut_icon={}
let g:dashboard_custom_shortcut_icon['last_session'] = ' '
let g:dashboard_custom_shortcut_icon['find_history'] = ' '
let g:dashboard_custom_shortcut_icon['find_file'] = ' '
let g:dashboard_custom_shortcut_icon['new_file'] = ' '
let g:dashboard_custom_shortcut_icon['change_colorscheme'] = ' '
let g:dashboard_custom_shortcut_icon['find_word'] = ' '
let g:dashboard_custom_shortcut_icon['book_marks'] = ' '

" Entries
let g:dashboard_custom_shortcut={
      \ 'new_file'           : 'SPC b e',
      \ 'last_session'       : 'SPC s l',
      \ 'book_marks'         : 'SPC f m',
      \ 'find_history'       : 'SPC f h',
      \ 'find_file'          : 'SPC f f',
      \ 'find_word'          : 'SPC f t',
      \ 'change_colorscheme' : 'SPC f S',
      \ }
