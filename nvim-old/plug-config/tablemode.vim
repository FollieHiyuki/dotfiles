let g:table_mode_corner='|' " Markdown-compatible
let g:table_mode_corner_corner='+' " ReST-compatible
let g:table_mode_header_fillchar='=' " ReST-compatible
let g:table_mode_delete_row_map='<Leader>td'
let g:table_mode_delete_column_map='<Leader>tc'
let g:table_mode_insert_column_after_map='<Leader>ti'
let g:table_mode_insert_column_before_map='<Leader>tk'
let g:table_mode_add_formula_map='<Leader>tf'
let g:table_mode_eval_expr_map='<Leader>te'
