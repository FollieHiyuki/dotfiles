let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

autocmd FileType gitcommit let b:EditorConfig_disable = 1
