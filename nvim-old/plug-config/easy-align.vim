" Settings
let g:easy_align_ignore_groups = ['Comment', 'String']
" let g:easy_align_bypass_fold = 1

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap <Leader>aa <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap <Leader>aa <Plug>(EasyAlign)
