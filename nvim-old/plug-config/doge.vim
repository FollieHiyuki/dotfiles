let g:doge_enable_mappings=0
let g:doge_mapping='<Leader>d'
let g:doge_buffer_mappings=0
let g:doge_mapping_comment_jump_forward='<A-f>'
let g:doge_mapping_comment_jump_backward='<A-b>'
let g:doge_comment_jump_modes = ['n', 'i', 's']

" Commands
" :DogeGenerate {doc_standard}
" :DogeCreateDocStandard {doc_standard}
