" Pandoc
let g:pandoc#filetypes#handled = ["pandoc", "markdown", "rst", "textile"]
let g:pandoc#filetypes#pandoc_markdown = 0
let g:pandoc#modules#disabled = ["folding"]
" Vimtex
let g:vimtex_compiler_progname = 'nvr'
let g:tex_flavor = 'latex'
" Markdown TOC
let g:vmt_fence_text = 'TOC'
let g:vmt_fence_closing_text = '/TOC'
let g:vmt_include_headings_before = 0
let g:vmt_list_item_char = '*'
" Markdown preview
let g:mkdp_refresh_slow = 1
let g:mkdp_page_title = '「${name}」'
