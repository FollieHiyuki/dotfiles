let g:startify_custom_header = [
  \ "   <-. (`-')_  (`-')  _                 (`-')  _     <-. (`-')  ",
  \ "      \\( OO) ) ( OO).-/     .->        _(OO ) (_)       \\(OO )_ ",
  \ "   ,--./ ,--/ (,------.(`-')----. ,--.(_/,-.\\ ,-(`-'),--./  ,-.)",
  \ "   |   \\ |  |  |  .---'( OO).-.  '\\   \\ / (_/ | ( OO)|   `.'   |",
  \ "   |  . '|  |)(|  '--. ( _) | |  | \\   /   /  |  |  )|  |'.'|  |",
  \ "   |  |\\    |  |  .--'  \\|  |)|  |_ \\     /_)(|  |_/ |  |   |  |",
  \ "   |  | \\   |  |  `---.  '  '-'  '\\-'\\   /    |  |'->|  |   |  |",
  \ "   `--'  `--'  `------'   `-----'     `-'     `--'   `--'   `--'",
  \ ]

let g:startify_session_dir = '$HOME/.local/share/nvim/sessions'

" returns all modified files of the current git repo
" `2>/dev/null` makes the command fail quietly, so that when we are not
" in a git repo, the list will be empty
function! s:gitModified()
    let files = systemlist('git ls-files -m 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction

" same as above, but show untracked files, honouring .gitignore
function! s:gitUntracked()
    let files = systemlist('git ls-files -o --exclude-standard 2>/dev/null')
    return map(files, "{'line': v:val, 'path': v:val}")
endfunction

let g:startify_lists = [
  \ { 'type': 'files',     'header': ['    Files']                  	    },
	\ { 'type': 'dir',       'header': ['    Current folder: '. getcwd()]  },
	\ { 'type': 'sessions',  'header': ['    Sessions']                    },
	\ { 'type': 'bookmarks', 'header': ['    Bookmarks']                   },
  \ { 'type': function('s:gitModified'),  'header': ['    Git modified'] },
  \ { 'type': function('s:gitUntracked'), 'header': ['    Git untracked']},
	\ ]

let g:startify_session_autoload = 1
let g:startify_session_delete_buffers = 1
let g:startify_change_to_vcs_root = 1
let g:startify_fortune_use_unicode = 1
let g:startify_session_persistence = 1
let g:startify_enable_special = 1
let g:webdevicons_enable_startify = 1
let g:startify_bookmarks = [
	\ { 'i': '~/.config/nvim/init.vim' },
	\ { 'p': '~/Code'},
	\ { 'c': '~/.config'},
	\ ]

function! StartifyEntryFormat()
	return 'WebDevIconsGetFileTypeSymbol(absolute_path) ." ". entry_path'
endfunction
