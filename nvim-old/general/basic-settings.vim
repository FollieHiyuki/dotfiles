syntax on
filetype indent plugin on
if &term =~ '256color'
  set t_ut=
endif
if has('termguicolors')
  set termguicolors
else
  set t_Co=256
endif
set background=dark
set guifont=Iosevka\ Nerd\ Font
set noerrorbells
set noshowmode
set encoding=utf-8
set fileencoding=utf-8
" set iskeyword+=- " treat - as part of a word
set formatoptions-=cro
set clipboard+=unnamedplus
set number relativenumber
" set cursorline
set tabstop=4 softtabstop=4 " 4 spaces for 1 tab
set shiftwidth=4 " 4 spaces for 1 indent
set smarttab
set expandtab " convert tabs to spaces
set smartindent
set autoindent
set splitbelow splitright
set nowrap
set whichwrap+=<,>,[,] ",h,l " wrap lines on h,l,arrows
set mouse=a
set ignorecase smartcase
set incsearch nohlsearch
set laststatus=2
set ruler
set noswapfile
set showtabline=2
set conceallevel=1
set pumheight=12 " height for completion box
set wildmenu
set wildmode=full

" POSIX compatible (bash is not truly ideal but ...)
if &shell =~# 'fish$'
    set shell=bash
endif

" Search files recursively
set path=.,,**

let mapleader = "\<Space>"
let g:netrw_banner = 0
let g:python3_host_prog = '/usr/bin/python3'
