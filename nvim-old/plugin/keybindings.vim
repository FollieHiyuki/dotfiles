" Better escape
inoremap jk <Esc>
inoremap kj <Esc>
" Better Y
nmap Y y$
" Basic file commands
nnoremap <A-f> :!touch<Space>
nnoremap <A-e> :!crf<Space>
nnoremap <A-d> :!mkdir<Space>
nnoremap <A-m> :!mv<Space>%<Space>
" Shorten default replace command
nnoremap <A-s> :%s//g<Left><Left>
" Delete trailing whitespace
nnoremap <A-w> :%s/\s\+$//e<CR>
" Use alt + hjkl to resize windows
nnoremap <silent> <A-j> :resize -2<CR>
nnoremap <silent> <A-k> :resize +2<CR>
nnoremap <silent> <A-h> :vertical resize -2<CR>
nnoremap <silent> <A-l> :vertical resize +2<CR>
" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" Terminal window navigation
" tnoremap <C-h> <C-\><C-N><C-w>h
" tnoremap <C-j> <C-\><C-N><C-w>j
" tnoremap <C-k> <C-\><C-N><C-w>k
" tnoremap <C-l> <C-\><C-N><C-w>l
tnoremap <Esc> <C-\><C-n>
" Cancel actions
nnoremap <silent> <C-c> <Esc>
inoremap <silent> <C-c> <Esc>i
" Move between buffers
nnoremap <silent> <TAB> :bnext<CR>
nnoremap <silent> <S-TAB> :bprevious<CR>
" Just do it
" cmap w!! w !sudo tee %
" Fix indenting visual block
vmap < <gv
vmap > >gv
" Move selected line / block of text in visual mode
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv
" Bad hands
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Qa! qa!
cnoreabbrev QA! qa!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
cnoreabbrev Qa qa
cnoreabbrev QA qa
cnoreabbrev Wqa wqa
cnoreabbrev WQa wqa

function! ToggleIndentStyle()
  if &expandtab == 1
    set noexpandtab
    set softtabstop&
    set shiftwidth&
    echomsg "Switched to indent with tabs."
  else
    set expandtab
    set softtabstop = 4
    set shiftwidth = 4
    echomsg "Switched to indent with 4 spaces."
  endif
endfunction
noremap <A-t> :call ToggleIndentStyle()<CR>
