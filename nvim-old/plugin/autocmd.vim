" No continuous commenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Correct comment highlighting in .json files
autocmd FileType json syntax match Comment +\/\/.\+$+
" Turn on spellcheck for markdown and tex files
autocmd BufNewFile,BufEnter,BufRead *.md,*.mkd setfiletype markdown
autocmd BufNewFile,BufEnter,BufRead *.tex setfiletype tex
augroup auto_spellcheck
  autocmd BufNewFile,BufRead *.md,*.mkd setlocal spell
  autocmd BufNewFile,BufRead *.tex setlocal spell
augroup END
" Edit binary files as hex in vim (taken from :h hex-editing)
" vim -b : edit binary using xxd-format!
" I might never use this feature -_- but :%!xxd and :%!xxd -r to toggle hex mode
augroup Binary
  au!
  au BufReadPre  *.bin,*.exe let &bin=1
  au BufReadPost *.bin,*.exe if &bin | %!xxd
  au BufReadPost *.bin,*.exe set ft=xxd | endif
  au BufWritePre *.bin,*.exe if &bin | %!xxd -r
  au BufWritePre *.bin,*.exe endif
  au BufWritePost *.bin,*.exe if &bin | %!xxd
  au BufWritePost *.bin,*.exe set nomod | endif
augroup END
