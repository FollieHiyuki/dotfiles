### Old Neovim config

This is my old Neovim config, using [vim-plug](https://github.com/junegunn/vim-plug) and [coc.nvim](https://github.com/neoclide/coc.nvim).

It is not maintained anymore, you should look for [the new config in Lua](../home/.config/nvim/init.lua) instead.

### Credits

- [@ChristianChiarulli](https://github.com/ChristianChiarulli)'s old Neovim config
