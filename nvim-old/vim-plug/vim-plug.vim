" Install vim-plug if missing
if empty(glob('$HOME/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

call plug#begin('$HOME/.local/share/nvim/plugged')

" Colorschemes
Plug 'arcticicestudio/nord-vim'
Plug 'joshdick/onedark.vim'
" Status bar
" Plug 'vim-airline/vim-airline'
Plug 'glepnir/spaceline.vim'
" Devicons
Plug 'ryanoasis/vim-devicons'
" Tabline
Plug 'bagrat/vim-buffet'
" Startscreen
Plug 'glepnir/dashboard-nvim'
" Plug 'mhinz/vim-startify'
"=================================================
" Syntax
" Plug 'linkinpark342/xonsh-vim'
Plug 'sheerun/vim-polyglot'
Plug 'mechatroner/rainbow_csv'
Plug 'jackguo380/vim-lsp-cxx-highlight'
" Indent guide
Plug 'Yggdroot/indentLine'
" Whitespaces
Plug 'ntpeters/vim-better-whitespace'
" Highlight cursor
Plug 'RRethy/vim-illuminate'
" Colorize color codes
Plug 'norcalli/nvim-colorizer.lua'
" Parenthesis
Plug 'luochen1990/rainbow'
"=================================================
" Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Linting
Plug 'dense-analysis/ale'
" Make
Plug 'tpope/vim-dispatch'
Plug 'ilyachur/cmake4vim'
" Snippets for coc-snippets
Plug 'honza/vim-snippets'
Plug 'dsznajder/vscode-es7-javascript-react-snippets', {'do': 'yarn install --frozen-lockfile && yarn compile'}
Plug 'sdras/vue-vscode-snippets'
Plug 'ashinzekene/vscode-nestjs-snippets'
Plug 'andys8/vscode-jest-snippets'
" Database support for coc-db
Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'
" Variables/Functions preview
Plug 'liuchengxu/vista.vim'
" Coding doc generator
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }
" Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'rhysd/git-messenger.vim'
" Github news
" Plug 'junegunn/vim-github-dashboard'
" Gist on Github
" Plug 'mattn/webapi-vim'
" Plug 'mattn/vim-gist'
" Maintain project directory
Plug 'airblade/vim-rooter'
"=================================================
" Split lines
Plug 'AndrewRadev/splitjoin.vim'
" Multi-cursor
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
" Comments
Plug 'tpope/vim-commentary'
" html tags, brackets
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'alvan/vim-closetag'
Plug 'Raimondi/delimitMate'
" Move around
Plug 'unblevable/quick-scope'
Plug 'justinmk/vim-sneak'
" Edit text in regions
" Plug 'chrisbra/NrrwRgn'
" Smooth scrolling
Plug 'psliwka/vim-smoothie'
" Alternative w,b,e
" Plug 'bkad/CamelCaseMotion'
" Spelling, dictionary
Plug 'echuraev/translate-shell.vim'
" Plug 'reedes/vim-lexical'
" Plug 'ron89/thesaurus_query.vim'
" Align text
Plug 'junegunn/vim-easy-align'
" editorconfig
Plug 'editorconfig/editorconfig-vim'
"=================================================
" Fuzzy finder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Nerdtree
" Plug 'preservim/nerdtree'
" Plug 'Xuyuanp/nerdtree-git-plugin'
" Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
" File managers
Plug 'kevinhwang91/rnvimr'
Plug 'vifm/vifm.vim'
Plug 'mcchrish/nnn.vim'
" Change windows
Plug 't9md/vim-choosewin'
" Emacs like which key
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
" Better embbed terminal
Plug 'voldikss/vim-floaterm'
" Default replacing is a pain
Plug 'brooth/far.vim'
" Undotree
Plug 'mbbill/undotree'
" Quickly close buffers
Plug 'Asheq/close-buffers.vim'
" Cheatsheet
Plug 'dbeniamine/cheat.sh-vim'
" Plug 'wlemuel/vim-tldr'
"=================================================
" Live-server for html
Plug 'turbio/bracey.vim', { 'do': 'npm install --prefix server' }
" Markdown
Plug 'mzlogin/vim-markdown-toc'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install' }
" Pandoc integration / LaTex support
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
" Plug 'lervag/vimtex'
" Org mode
" Plug 'jceb/vim-orgmode'
" Writing mode
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
" Note taking
Plug 'vimwiki/vimwiki'
" Tables
Plug 'dhruvasagar/vim-table-mode'

call plug#end()

" Install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
