#!/usr/bin/env bash

# get current branch in git repo
parse_git_branch() {
  BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${BRANCH}" = "" ]
	then
		STAT=$(parse_git_dirty)
		echo "${BRANCH}${STAT}"
	else
		echo ""
	fi
}

# get current status of git repo
parse_git_dirty() {
	status=$(git status 2>&1 | tee)
	dirty=$(echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?")
	untracked=$(echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?")
	ahead=$(echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?")
	newfile=$(echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?")
	renamed=$(echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?")
	deleted=$(echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?")
	bits=''
	if [ "${ahead}" = "0" ]; then
		bits="*${bits}"
	fi
	if [ "${renamed}" = "0" ]; then
		bits=">${bits}"
	fi
	if [ "${newfile}" = "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" = "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" = "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" = "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" = "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

export PS1="\[$(tput bold)\]\[$(tput setaf 1)\]╭─[\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \$(if [[ \$? == 0 ]]; then echo \"\[$(tput setaf 2)\]\342\234\223\"; else echo \"\[$(tput setaf 1)\]\342\234\227\"; fi)\[$(tput setaf 1)\]] \[$(tput setaf 5)\]\w \[$(tput setaf 2)\]\`parse_git_branch\`\n\[$(tput setaf 1)\]╰\[$(tput sgr0)\] "
