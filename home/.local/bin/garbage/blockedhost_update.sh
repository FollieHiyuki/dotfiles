#!/bin/sh

# Blocked hosts for Qutebrowser
# run this as crontab to update blocked hosts list regularly (daily)
# eg: @weekly ~/.local/bin/garbage/blockedhost_update.sh

curl -fsLo ~/.config/qutebrowser/blockedHosts https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/thirdparties/www.malwaredomainlist.com/hostslist/hosts.txt
curl -fs https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/thirdparties/www.malwaredomainlist.com/hostslist/hosts.txt >> ~/.config/qutebrowser/blockedHosts
curl -fs https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/thirdparties/hosts-file.net/ad_servers.txt >> ~/.config/qutebrowser/blockedHosts
