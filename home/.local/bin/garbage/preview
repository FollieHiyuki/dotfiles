#!/bin/sh

# For using with fzf
[ -d "$HOME/.cache/fzf" ] || mkdir -p "$HOME/.cache/fzf"

X="2"
Y="1"
WIDTH="$FZF_PREVIEW_COLUMNS"
HEIGHT="$FZF_PREVIEW_LINES"
TMP="$HOME/.cache/fzf/thumbnail.$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | awk '{print $1}')"

# Handy functions for convenience
preview_img() {
  if [ "$TERM" = "xterm-kitty" ]
  then
    [ ! -f "${TMP}.jpg" ] && convert "$1"'[0]' "${TMP}.jpg"
    kitty icat --silent --place=${WIDTH}x${HEIGHT}@${X}x${Y} "${TMP}.jpg"
  else
    mediainfo "$1" || exiftool "$1" || identify "$1"
  fi
}

preview_vid() {
  if [ "$TERM" = "xterm-kitty" ]
  then
    [ ! -f "${TMP}.jpg" ] && ffmpegthumbnailer -i "$1" -o "${TMP}.jpg" -s 0
    kitty icat --silent --place=${WIDTH}x${HEIGHT}@${X}x${Y} "${TMP}"
  else
    mediainfo "$1" || exiftool "$1" || fprobe -pretty "$1" 2>&1
  fi
}

# Clear the old image before processing, otherwise shit happens
if [ "$TERM" = "xterm-kitty" ]
then
  kitty icat --silent --clear
fi

case "$1" in
  # First check common extensions
  *.png|*.jpg|*.bmp|*.jpeg|*.gif|*.xpm)
    if [ "$TERM" = "xterm-kitty" ]
    then
      kitty icat --silent --place=${WIDTH}x${HEIGHT}@${X}x${Y} "$1"
    else
      mediainfo "$1" || exiftool "$1" || identify "$1"
    fi ;;
  *.wav|*.mp3|*.flac|*.m4a|*.ape|*.ac3|*.og[agx]|*.spx|*.dsf|*.opus|*.dff|*.wma|*.wvc?) mediainfo "$1" || exiftool "$1" ;;
  *.avi|*.mp4|*.wmv|*.dat|*.3gp|*.vob|*.ogv|*.mkv|*.mpe?g|*.fl[icv]|*.m2v|*.webm|*.m?ts|*.r[am]|*.qt|*.divx|*.as[fx]|*.m4v|*.mov) preview_vid "$1" ;;
  *.pdf) mutool draw -F txt -i -- "$1" 1-10 || pdftotext -l 10 -nopgbrk -q -- "$1" - || exiftool "$1" ;;
  *.ps) pstotext "$1" || ps2ascii "$1" ;;
  *.epub|*.fb2) pandoc -s -t markdown -- "$1" ;;
  *.djvu) djvutxt "$1" || exiftool "$1" ;;
  *.html|*.xhtml|*.htm) pandoc -s -t markdown -- "$1" || lynx -dump -- "$1" ;;
  *.mkd|*.md|*.markdown) mdcat "$1" || glow -s dark "$1" ;;
  *.ipynb) notedown --from notebook "$1" --to markdown | pandoc -f markdown -t plain ;;
  *.torrent) dumptorrent -v "$1" || transmission-show -- "$1" ;;
  *.zip|*.jar|*.war|*.ear|*.oxt) zip -sf "$1" || zipinfo "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.tar) tar -tvf "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.tgz|*.tar.gz) tar -tvf "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.tbz2|*.tar.bz2) tar -tvjf "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.tar.txz|*.txz) xz --list "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.rar) unrar lt -p- -- "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.7z) 7z l -p -- "$1" || atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
  *.iso) isoinfo -l -i "$1" ;;
  *.doc) catdoc -- "$1" ;;
  *.docx) pandoc -s -t markdown -- "$1" ;;
  *.odt|*.ott|*.s[xt]w|*.sxc) pandoc -s -t markdown -- "$1" || odt2txt "$1" ;;
  *.xls) xls2csv -- "$1" ;;
  *.xlsx) xlsx2csv -- "$1" ;;
  *.ods|*.odp|*.sxw) pandoc -s -t markdown -- "$1" ;;
  *.json) jq --color-output . "$1" || python -m json.tool -- "$1" ;;
  # Then fallback to mimetypes
  *)
    mimetype=$(file --dereference --brief --mime-type -- "$1")
    case "$mimetype" in
      inode/directory)
        exa -1a --sort=type --color always --icons "$1" ;;
      application/zip|application/x-tar|application/x-gzip)
        atool -l -q "$1" | tail -n +3 | awk -F'   ' '{print $NF}' ;;
      image/vnd.djvu)
        djvutxt "$1" || exiftool "$1" ;;
      image/*)
        preview_img "$1" ;;
      application/font*|application/*opentype|font/*)
        if [ "$TERM" = "xterm-kitty" ]
        then
          [ ! -f "${TMP}.jpg" ] && genfontimage "$1" "${TMP}.jpg"
          kitty icat --silent --place=${WIDTH}x${HEIGHT}@${X}x${Y} "${TMP}.jpg"
        else
          echo '----- Font Classification -----' && file --dereference --brief -- "$1"
        fi ;;
      video/*)
        preview_vid "$1" ;;
      text/* | */xml)
        bat --style plain --color=always "$1" ;;
      *)
        echo '----- File Type Classification -----' && file --dereference --brief -- "$1" ;;
    esac ;;
esac
