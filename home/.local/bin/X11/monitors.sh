#!/bin/sh

status=$(xrandr | grep " connected")

# Built-int monitor
xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal

if echo "$status" | grep "HDMI-1"; then
    xrandr --output HDMI-1 --mode 1920x1080 --pos 1920x0 --rotate normal
else
    xrandr --output HDMI-1 --off
fi

if echo "$status" | grep "DP-2"; then
    xrandr --output DP-2 --mode 1920x1080 --pos -1920x0 --rotate normal
else
    xrandr --output DP-2 --off
fi
