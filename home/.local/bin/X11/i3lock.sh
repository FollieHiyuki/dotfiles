#!/bin/sh

i3lock --nofork           \
  --indicator             \
  --keylayout 0           \
  --clock                 \
  --layoutcolor=ffffff80  \
  --timecolor=ffffff80    \
  --datecolor=ffffff80    \
  --ignore-empty-password \
  --show-failed-attempts  \
  --tiling                \
  --image="$HOME/Pictures/Wallpapers/9697jd.png"
