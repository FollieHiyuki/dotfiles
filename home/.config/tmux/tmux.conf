# delay between prefix and command
set -s escape-time 0

# start with 1 instead of 0
set -g base-index 1
set -g pane-base-index 1

# increase scrollback buffer size
set -g history-limit 50000

# tmux messages are displayed for 4 seconds
set -g display-time 4000

# refresh 'status-left' and 'status-right' more often
set -g status-interval 5

# upgrade $TERM for 24-bit colors
set -g default-terminal "screen-256color"
set-option -ga terminal-overrides ",screen-256color:Tc"

# no vim keys -_-
set -g status-keys emacs

# focus events enabled for terminals that support them
set -g focus-events on

# enable mouse
set -g mouse on

# super useful when using "grouped sessions" and multi-monitor setup
setw -g aggressive-resize on

# ______________              _____
# ___  __ \__  /___  ________ ___(_)______________
# __  /_/ /_  /_  / / /_  __ `/_  /__  __ \_  ___/
# _  ____/_  / / /_/ /_  /_/ /_  / _  / / /(__  )
# /_/     /_/  \__,_/ _\__, / /_/  /_/ /_//____/
#                     /____/
#
# Custom plugins' settings
set -g @prefix_highlight_show_copy_mode 'on'
set -g @prefix_highlight_show_sync_mode 'on'
set -g @prefix_highlight_prefix_prompt 'Wait'
set -g @prefix_highlight_copy_prompt 'Copy'
set -g @prefix_highlight_sync_prompt 'Sync'
# set -g @tmux_power_theme '#61AFEF'
# set -g @tmux_power_prefix_highlight_pos 'R'

# Load plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'arcticicestudio/nord-tmux'
# set -g @plugin "wfxr/tmux-power"
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'sainnhe/tmux-fzf'

run '~/.config/tmux/plugins/tpm/tpm'
