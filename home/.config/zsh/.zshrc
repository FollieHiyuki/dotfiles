# Install Zinit if missing
[ -d $HOME/.local/share/zsh/zinit/bin ] || git clone https://github.com/zdharma/zinit.git $HOME/.local/share/zsh/zinit/bin
# Load zinit
source $HOME/.local/share/zsh/zinit/bin/zinit.zsh

# Load ls_colors
autoload -U colors && colors

# disable in favor of zinit built-in method to add completions
[ -d $HOME/.local/share/zsh/completions ] || mkdir -p $HOME/.local/share/zsh/completions
# fpath+=$HOME/.local/share/zsh/completions

# Initialize completion
autoload -Uz compinit
zmodload zsh/complist
compinit -d $HOME/.local/share/zsh/zcompdump-$ZSH_VERSION

# Fun zsh stuff
autoload -U zmv zcalc zargs zed

# github-cli completion
# if command -v gh > /dev/null && [ ! -f $HOME/.local/share/zsh/completions/_gh ]; then
#   gh completion -s zsh > $HOME/.local/share/zsh/completions/_gh
# fi
# zinit ice blockf as"completion"
# zinit snippet $HOME/.local/share/zsh/completions/_gh

# kitten from kitty
if command -v kitty > /dev/null; then
  kitty + complete setup zsh | source /dev/stdin
fi

# starship prompt
if command -v starship > /dev/null; then
  eval "$(starship init zsh --print-full-init)"
fi

# direnv
if command -v direnv > /dev/null; then
  eval "$(direnv hook zsh)"
fi

# zoxide
if command -v zoxide >/dev/null; then
  eval "$(zoxide init zsh)"
fi

# poetry
# if [ ! -d $POETRY_HOME ]; then
#   curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
# fi
# if [ ! -f $HOME/.local/share/zsh/completions/_poetry ]; then
#   poetry completions zsh > $HOME/.local/share/zsh/completions/_poetry
# fi
# zinit ice blockf as"completion"
# zinit snippet $HOME/.local/share/zsh/completions/_poetry

# pyenv
# if [ ! -d $PYENV_ROOT ]; then
#    git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT
#    git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_ROOT/plugins/pyenv-virtualenv
#    git clone https://github.com/pyenv/pyenv-update.git $PYENV_ROOT/plugins/pyenv-update
# fi
# if [[ -o login ]]; then
#   eval "$(pyenv init --path)"
# fi
# eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"

# nodenv
# if [ ! -d $NODENV_ROOT ]; then
#   git clone https://github.com/nodenv/nodenv.git $NODENV_ROOT
#   mkdir -p $NODENV_ROOT/plugins
#   git clone https://github.com/nodenv/node-build.git $NODENV_ROOT/plugins/node-build
#   git clone https://github.com/nodenv/nodenv-env.git $NODENV_ROOT/plugins/nodenv-env
#   git clone https://github.com/nodenv/nodenv-man.git $NODENV_ROOT/plugins/nodenv-man
#   git clone https://github.com/nodenv/nodenv-aliases.git $NODENV_ROOT/plugins/nodenv-aliases
#   git clone https://github.com/nodenv/nodenv-each.git $NODENV_ROOT/plugins/nodenv-each
#   git clone https://github.com/nodenv/nodenv-update.git $NODENV_ROOT/plugins/nodenv-update
#   git clone https://github.com/nodenv/nodenv-vars.git $NODENV_ROOT/plugins/nodenv-vars
# fi
# eval "$(nodenv init -)"

# autostart gpg-agent
if ! pgrep -u "$USER" gpg-agent >/dev/null; then
  gpg-agent --daemon --enable-ssh-support >/dev/null
fi
if [[ -z "$SSH_AUTH_SOCK" ]]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
gpg-connect-agent updatestartuptty /bye >/dev/null

# Additional configurations (shamelessly taken from https://github.com/hlissner/dotfiles)
source $ZDOTDIR/config.zsh
source $ZDOTDIR/completion.zsh
source $ZDOTDIR/keybinds.zsh
source $ZDOTDIR/aliases.zsh

# ______________              _____
# ___  __ \__  /___  ________ ___(_)______________
# __  /_/ /_  /_  / / /_  __ `/_  /__  __ \_  ___/
# _  ____/_  / / /_/ /_  /_/ /_  / _  / / /(__  )
# /_/     /_/  \__,_/ _\__, / /_/  /_/ /_//____/
#                     /____/
# Some OMZ stuff
zinit wait lucid for \
    OMZ::lib/clipboard.zsh
zinit ice blockf as"completion" for \
    OMZ::plugins/docker/_docker \
    OMZ::plugins/rust/_rust \
    OMZ::plugins/fd/_fd

# fzf key bindings and zsh completions for some commands
zinit snippet https://github.com/junegunn/fzf/blob/master/shell/key-bindings.zsh
zinit snippet https://github.com/junegunn/fzf/blob/master/shell/completion.zsh
export FZF_COMPLETION_OPTS='-x'

# Better way to pass colors than asci codes (need to be loaded before other plugins)
zinit light zpm-zsh/colors

# fzf as tab-completion (need to be loaded before zsh-autosuggestions and fast-syntax-highlighting)
zinit light Aloxaf/fzf-tab

# Fish-like history suggestion (loaded on demanded for faster startup)
zinit ice wait lucid atload'_zsh_autosuggest_start'
zinit light zsh-users/zsh-autosuggestions

# More completions
zinit wait lucid blockf light-mode for \
  zsh-users/zsh-completions \
  lukechilds/zsh-better-npm-completion
# zinit light Aloxaf/gencomp

# Syntax
zinit ice wait lucid atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay"
zinit light zdharma/fast-syntax-highlighting

# Fish-like history search
zinit light zsh-users/zsh-history-substring-search

# Autopair plugin from the doomer
zinit light hlissner/zsh-autopair

# Git thingy
zinit light wfxr/forgit

# Abbreviations
zinit light olets/zsh-abbr
# zinit light djui/alias-tips

# Plugins' configurations
source $ZDOTDIR/plugins.zsh

$HOME/.local/bin/bunny
umask 0027
