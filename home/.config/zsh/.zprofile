# PATH
typeset -U PATH path
path=("$HOME/.local/bin" "$HOME/.local/bin/fzf" "$NODENV_ROOT/bin" "$PYENV_ROOT/bin" "$POETRY_HOME/bin" "$CARGO_HOME/bin" "$GOPATH/bin" "$NPM_CONFIG_PREFIX/bin" "$path[@]")
export PATH

# if [[ "$(tty)" = "/dev/tty1" ]]; then
#   exec startx $HOME/.config/X11/xinitrc
# fi
