# remap
alias x=extract
alias p="pulsemixer"
alias startx="startx $HOME/.config/X11/xinitrc"
alias fehwpp="feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/*"
alias ls="exa -lF --icons --sort=type"
alias la="exa -laF --icons --sort=type"
alias lt="exa --tree"
alias cp="cp -vir"
alias mv="mv -vi"
alias rm="rm -vr"
alias mkdir="mkdir -pv"
alias no="grep -viP"
alias yarn="yarn --use-yarnrc $HOME/.config/yarn/config"
alias tmux="TERM=screen-256color tmux"
# colorizing
alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"
alias diff="diff --color=auto"
alias ncdu="ncdu --color=dark"
alias ip="ip -color=always"
# vim
alias v="nvim"
alias vi='fzf | xargs -r nvim -o'
# emacs
alias doom="$HOME/.config/emacs-config/doom/bin/doom"
alias e="emacs -nw"
alias em="emacsclient -c -a 'emacs'"
# rsync
alias rsyncdir="rsync -avP --delete-after"
# youtube-dl
alias yoump3="youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail"
alias youflac="youtube-dl --extract-audio --audio-format flac"
alias youlist="youtube-dl --yes-playlist"

# Lazy cd-ing
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# colorize man pages
man() {
    MANPATH=$HOME/.local/share/man:$MANPATH \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;36m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man $@
}

# Search zshall for a special keyword
zman() {
    PAGER="less -g -s '+/^       "$1"'" man ${2:-zshall}
}

# nnn with cd on quit
if command -v nnn >/dev/null; then
    alias ncp="cat ${NNN_SEL:-${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection} | tr '\0' '\n'"

    n () {
        # Block nesting of nnn in subshells
        if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
            echo "nnn is already running"
            return
        fi

        # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
        # To cd on quit only on ^G, remove the "export" as in:
        #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
        # NOTE: NNN_TMPFILE is fixed, should not be modified
        export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

        # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
        # stty start undef
        # stty stop undef
        # stty lwrap undef
        # stty lnext undef

        nnn "$@"

        if [ -f "$NNN_TMPFILE" ]; then
                . "$NNN_TMPFILE"
                rm -f "$NNN_TMPFILE" > /dev/null
        fi
    }

    # nnn in dual pane mode (tmux)
    n2() {
      tmux new-session -d -s nnn -n nnn "nnn -Hc"
      tmux split-window -h "nnn -Hc"
      TERM=screen-256color tmux attach -t nnn:nnn
    }
fi

# typing speed testing
tt() {
  iconv -f UTF-8 -t ASCII "$1" | sed 's/--/-/g' \
      | sed '/^$/d' | sed 's/^[ \t]*//' | sed 's/\(^.\{1,80\}\).*/\1/' \
      > /tmp/tt.txt
  command tt /tmp/tt.txt
}
