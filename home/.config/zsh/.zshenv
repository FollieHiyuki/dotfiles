# __________
# ___  ____/_________   __
# __  __/  __  __ \_ | / /
# _  /___  _  / / /_ |/ /
# /_____/  /_/ /_/_____/
#
# XDG thingy
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_DATA_DIRS=$HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share
# alternate paths
export LESSHISTFILE=-
export MOST_INITFILE=$XDG_CONFIG_HOME/mostrc
export TERMINFO="$XDG_DATA_HOME/terminfo"
export TERMINFO_DIRS="$XDG_DATA_HOME/terminfo:/usr/share/terminfo"
export GOPATH=$XDG_DATA_HOME/go
export GRADLE_USER_HOME=$XDG_DATA_HOME/gradle
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java
export NODE_REPL_HISTORY=$XDG_CACHE_HOME/node_repl_history
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export NPM_CONFIG_PREFIX="$XDG_DATA_HOME/npm-global"
export CARGO_HOME=$XDG_DATA_HOME/cargo
export RUSTUP_HOME=$XDG_DATA_HOME/rustup
export BUNDLE_USER_CONFIG=$XDG_CONFIG_HOME/bundle
export BUNDLE_USER_CACHE=$XDG_CACHE_HOME/bundle
export BUNDLE_USER_PLUGIN=$XDG_DATA_HOME/bundle
export GEM_HOME=$XDG_DATA_HOME/gem
export GEM_SPEC_CACHE=$XDG_CACHE_HOME/gem
export DOCKER_CONFIG=$XDG_CONFIG_HOME/docker
export GNUPGHOME=$XDG_DATA_HOME/gnupg
export IPYTHONDIR=$XDG_CONFIG_HOME/ipython
export JUPYTER_CONFIG_DIR=$XDG_CONFIG_HOME/jupyter
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
export XINITRC=$XDG_CONFIG_HOME/X11/xinitrc
export XSERVERRC=$XDG_CONFIG_HOME/X11/xserverrc
export WGETRC=$XDG_CONFIG_HOME/wget/wgetrc
export WEECHAT_HOME=$XDG_CONFIG_HOME/weechat
export RIPGREP_CONFIG_PATH=$XDG_CONFIG_HOME/ripgrep/config

# env
export PAGER="less -R"
export VISUAL=nvim
export EDITOR=nvim
export SVDIR=$XDG_DATA_HOME/service
export _JAVA_AWT_WM_NONREPARENTING=1
export GPG_TTY="$(tty)"
export GOPROXY=direct
# fzf
export FZF_DEFAULT_OPTS="--multi --layout=reverse --inline-info
 --bind alt-j:preview-down,alt-k:preview-up
 --color fg:#D8DEE9,bg:#2E3440,hl:#A3BE8C,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C
 --color pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#81A1C1,marker:#EBCB8B"
# export FZF_DEFAULT_OPTS="--multi --layout=reverse --inline-info
#  --bind alt-j:preview-down,alt-k:preview-up
#  --color fg:-1,bg:-1,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe
#  --color info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef"
export FZF_DEFAULT_COMMAND="fd --type f --follow --hidden --exclude .git"
export FZF_CTRL_T_OPTS="--no-height --preview-window 'left:60%' --preview '$HOME/.local/bin/garbage/preview {} 2>/dev/null'"
export FZF_CTRL_T_COMMAND="fd --follow --hidden --exclude .git"
export FZF_ALT_C_OPTS="--preview 'exa -1a --sort=type --color always --icons {} 2>/dev/null'"
export FZF_ALT_C_COMMAND="fd --type d --follow --hidden --exclude .git"
export FZF_TMUX=1
# .NET
export DOTNET_CLI_TELEMETRY_OPTOUT=1
# nodenv
export NODENV_ROOT=$XDG_DATA_HOME/nodenv
# pyenv
export PYENV_ROOT=$XDG_DATA_HOME/pyenv
# poetry
export POETRY_HOME=$XDG_DATA_HOME/poetry
# zoxide
export _ZO_DATA_DIR="$HOME/.local/share/zoxide"
export _ZO_ECHO=1
export _ZO_FZF_OPTS="$FZF_DEFAULT_OPTS --no-multi"
export _ZO_RESOLVE_SYMLINKS=1
# cht.sh
export CHTSH="$XDG_CONFIG_HOME/cht.sh"
# nnn
if command -v nnn >/dev/null
then
  export TERMINAL=alacritty # for the preview script
  export NNN_OPTS="HUedx"
  export NNN_PLUG='b:bulknew;c:fzcd;o:fzopen;z:fzz;d:gpgd;e:gpge;h:hexview;m:nmount;n:nuke;t:preview-tui;r:renamer;p:rsynccp;j:splitjoin;s:suedit;i:ringtone'
  export NNN_FIFO="/tmp/nnn.fifo"
  export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
  # export NNN_OPENER="$XDG_CONFIG_HOME/nnn/plugins/nuke"
  # export NNN_TRASH=1
  export USE_SCOPE=1
  export NNN_COLORS='2345'
  export NNN_FCOLORS='c1e20402006006f7c6d6ab01'
  export NNN_BMS="h:~;C:~/Code;D:~/Downloads;P:~/Pictures;V:~/Videos;A:~/Media;U:~/Music;f:~/.config;l:~/.local/share;e:/etc;u:/usr/share;o:/opt;b:/boot;m:/media;M:/mnt;i:/run/media/$USER;v:/var;t:/tmp;d:/dev;s:/srv;r:/"
  export NNN_SSHFS="sshfs -o reconnect,idmap=user,follow_symlinks"
fi
# lf
if command -v lf >/dev/null
then
  export LF_ICONS="\
  tw=:\
  st=:\
  ow=:\
  dt=:\
  di=:\
  fi=:\
  ln=:\
  or=:\
  ex=:\
  *.7z=:\
  *.a=:\
  *.ace=:\
  *.alz=:\
  *.ai=:\
  *.apk=:\
  *.arc=:\
  *.arj=:\
  *.asm=:\
  *.asp=:\
  *.aup=:\
  *.avi=:\
  *.awk=:\
  *.bash=:\
  *.bat=:\
  *.bmp=:\
  *.bz=:\
  *.bz2=:\
  *.c=:\
  *.c++=:\
  *.cab=:\
  *.cbr=:\
  *.cbz=:\
  *.cc=:\
  *.class=:\
  *.clj=:\
  *.cljc=:\
  *.cljs=:\
  *.cmake=:\
  *.cmd=:\
  *.coffee=:\
  *.conf=:\
  *.cp=:\
  *.cpio=:\
  *.cpp=:\
  *.cs=:\
  *.csh=:\
  *.css=:\
  *.cue=:\
  *.cvs=:\
  *.cxx=:\
  *.d=:\
  *.dart=:\
  *.db=:\
  *.deb=:\
  *.diff=:\
  *.dll=:\
  *.doc=:\
  *.docx=:\
  *.dump=:\
  *.dwm=:\
  *.dz=:\
  *.edn=:\
  *.eex=:\
  *.efi=:\
  *.ejs=:\
  *.elf=:\
  *.elm=:\
  *.epub=:\
  *.ear=:\
  *.erl=:\
  *.esd=:\
  *.ex=:\
  *.exe=:\
  *.exs=:\
  *.f#=:\
  *.fifo=|:\
  *.fish=:\
  *.flac=:\
  *.flv=:\
  *.fs=:\
  *.fsi=:\
  *.fsscript=:\
  *.fsx=:\
  *.gem=:\
  *.gemspec=:\
  *.gif=:\
  *.go=:\
  *.gz=:\
  *.gzip=:\
  *.h=:\
  *.haml=:\
  *.hbs=:\
  *.hh=:\
  *.hpp=:\
  *.hrl=:\
  *.hs=:\
  *.htaccess=:\
  *.htm=:\
  *.html=:\
  *.htpasswd=:\
  *.hxx=:\
  *.ico=:\
  *.img=:\
  *.ini=:\
  *.iso=:\
  *.jar=:\
  *.java=:\
  *.jl=:\
  *.jpeg=:\
  *.jpg=:\
  *.js=:\
  *.json=:\
  *.jsx=:\
  *.key=:\
  *.ksh=:\
  *.leex=:\
  *.less=:\
  *.lha=:\
  *.lhs=:\
  *.log=:\
  *.lrz=:\
  *.lua=:\
  *.lz=:\
  *.lz4=:\
  *.lzh=:\
  *.lzma=:\
  *.lzo=:\
  *.m2v=:\
  *.m4a=:\
  *.m4v=:\
  *.markdown=:\
  *.md=:\
  *.mdx=:\
  *.mjpeg=:\
  *.mjpg=:\
  *.mjs=:\
  *.mkv=:\
  *.ml=λ:\
  *.mli=λ:\
  *.mng=:\
  *.mov=:\
  *.mp3=:\
  *.mp4=:\
  *.mp4v=:\
  *.mpeg=:\
  *.mpg=:\
  *.msi=:\
  *.mustache=:\
  *.nix=:\
  *.o=:\
  *.odt=:\
  *.ods=:\
  *.odp=:\
  *.ogg=:\
  *.pdf=:\
  *.php=:\
  *.pl=:\
  *.pm=:\
  *.png=:\
  *.pp=:\
  *.ppt=:\
  *.pptx=:\
  *.pro=:\
  *.ps1=:\
  *.psb=:\
  *.pub=:\
  *.py=:\
  *.pyc=:\
  *.pyd=:\
  *.pyo=:\
  *.r=ﳒ:\
  *.rake=:\
  *.rar=:\
  *.rb=:\
  *.rc=:\
  *.rlib=:\
  *.rmd=:\
  *.rom=:\
  *.rpm=:\
  *.rproj=鉶:\
  *.rs=:\
  *.rss=:\
  *.rtf=:\
  *.rz=:\
  *.s=:\
  *.sar=:\
  *.sass=:\
  *.scala=:\
  *.scss=:\
  *.sh=:\
  *.slim=:\
  *.sln=:\
  *.so=:\
  *.sql=:\
  *.styl=:\
  *.suo=:\
  *.svg=:\
  *.swift=:\
  *.swm=:\
  *.t=:\
  *.t7z=:\
  *.tar=:\
  *.taz=:\
  *.tbz=:\
  *.tbz2=:\
  *.tex=ﭨ:\
  *.tgz=:\
  *.tif=:\
  *.tiff=:\
  *.tlz=:\
  *.toml=:\
  *.ts=:\
  *.tsx=:\
  *.txz=:\
  *.tz=:\
  *.tzo=:\
  *.tzst=:\
  *.twig=:\
  *.vifm=:\
  *.vim=:\
  *.vimrc=:\
  *.vob=:\
  *.vue=﵂:\
  *.wav=:\
  *.war=:\
  *.webm=:\
  *.webmanifest=:\
  *.webp=:\
  *.wim=:\
  *.xbm=:\
  *.xbps=:\
  *.xcplayground=:\
  *.xhtml=:\
  *.xls=:\
  *.xlsx=:\
  *.xml=:\
  *.xpm=:\
  *.xul=:\
  *.xz=:\
  *.yaml=:\
  *.yml=:\
  *.z=:\
  *.zip=:\
  *.zoo=:\
  *.zsh=:\
  *.zip=:\
  *.zst=:\
  "
fi

# ______________              _____
# ___  __ \__  /___  ________ ___(_)______________
# __  /_/ /_  /_  / / /_  __ `/_  /__  __ \_  ___/
# _  ____/_  / / /_/ /_  /_/ /_  / _  / / /(__  )
# /_/     /_/  \__,_/ _\__, / /_/  /_/ /_//____/
#                     /____/
# Zinit
declare -A ZINIT
export ZINIT[HOME_DIR]=$HOME/.local/share/zsh/zinit
export ZINIT[BIN_DIR]=$HOME/.local/share/zsh/zinit/bin
export ZINIT[PLUGINS_DIR]=$HOME/.local/share/zsh/zinit/plugins
export ZINIT[COMPLETIONS_DIR]=$HOME/.local/share/zsh/zinit/completions
export ZINIT[SNIPPETS_DIR]=$HOME/.local/share/zsh/zinit/snippets
export ZINIT[ZCOMPDUMP_PATH]=$HOME/.local/share/zsh/zinit/zcompdump
export ZINIT[MUTE_WARNINGS]=0
export ZINIT[OPTIMIZE_OUT_DISK_ACCESSES]=0
export ZPFX=$HOME/.local/share/zsh/zinit/polaris
# Alias-tips
export ZSH_PLUGINS_ALIAS_TIPS_EXPAND=1
export ZSH_PLUGINS_ALIAS_TIPS_TEXT="💡 => "
# zsh-autosuggestions
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
export ZSH_AUTOSUGGEST_USE_ASYNC=1
# zsh-abbr
export ABBR_USER_ABBREVIATIONS_FILE="$HOME/.local/share/zsh/abbreviations"
# gencomp
export GENCOMP_DIR=$XDG_DATA_HOME/zsh/completions
# forgit
if [ -n "$XDG_SESSION_TYPE" ]; then
  if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export FORGIT_COPY_CMD="wl-copy"
  else
    if command -v xclip >/dev/null; then
      export FORGIT_COPY_CMD="xclip -selection clipboard"
    else
      export FORGIT_COPY_CMD="xsel -i -b"
    fi
  fi
fi
