/*******************************************************************************
 * ROFI Color theme
 * Based on Nord colorscheme
 *******************************************************************************/

* {
    spacing:                     2;
    border-color:                @blue;
    separatorcolor:              #4c566a;
    blue:                        #81a1c1;
    foreground:                  #d8dee9;
    normal-foreground:           @foreground;
    active-foreground:           @foreground;
    urgent-foreground:           @background;
    selected-normal-foreground:  @background;
    selected-active-foreground:  #d08770;
    selected-urgent-foreground:  @background;
    alternate-normal-foreground: @foreground;
    alternate-active-foreground: @active-foreground;
    alternate-urgent-foreground: @urgent-foreground;
    background:                  #2e3440;
    normal-background:           @background;
    active-background:           @background;
    urgent-background:           #ebcb8b;
    background-color:            @background;
    selected-normal-background:  @blue;
    selected-active-background:  @blue;
    selected-urgent-background:  #bf616a;
    alternate-normal-background: @background;
    alternate-urgent-background: @urgent-background;
    alternate-active-background: @background;
}
window {
    background-color: @background;
    border:           2;
    padding:          5;
    width:            25%;
}
mainbox {
    border:  0;
    padding: 0;
}
message {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    padding:      1px ;
}
textbox {
    text-color: @foreground;
}
listview {
    fixed-height: 0;
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    spacing:      4px ;
    scrollbar:    false;
    padding:      2px 0px 0px ;
}
element {
    border:  0;
    padding: 3px ;
}
element.normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}
element.normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}
element.normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}
element.selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}
element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}
element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}
element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}
element.alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}
element-icon {
  size: 20px;
}
scrollbar {
    width:        4px ;
    border:       0;
    handle-color: @normal-foreground;
    handle-width: 8px ;
    padding:      0;
}
mode-switcher {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
}
button {
    spacing:    0;
    text-color: @normal-foreground;
}
button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}
inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    padding:    1px ;
}
case-indicator {
    spacing:    0;
    text-color: @normal-foreground;
}
entry {
    spacing:    0;
    text-color: @normal-foreground;
}
prompt {
    spacing:    0;
    text-color: @normal-foreground;
}
inputbar {
    children:   [ prompt,textbox-prompt-colon,entry];
}
textbox-prompt-colon {
    expand:     false;
    str:        ":";
    margin:     0px 0.3em 0em 0em ;
    text-color: @normal-foreground;
}
