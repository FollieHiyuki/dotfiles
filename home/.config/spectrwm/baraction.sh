#!/bin/sh

_backlight() {
    $HOME/.local/bin/info/backlight
}

_tempamd() {
    $HOME/.local/bin/info/tempamd
}

_tempintel() {
    $HOME/.local/bin/info/tempintel
}

_protonvpn() {
    $HOME/.local/bin/info/vpn_show
}

_ibusstate() {
    ibus engine | cut -d':' -f2
}

_cpu() {
    $HOME/.local/bin/info/cpu
}

_ram() {
    $HOME/.local/bin/info/ram
}

_pulseaudio() {
    $HOME/.local/bin/info/pulse
}

_alsa() {
    amixer get Master | sed -nre 's/.*\\[off\\].*/婢 muted/p; s/.*\\[(.*%)\\].*/墳 \\1/p'
}

_battery() {
    $HOME/.local/bin/info/battery
}

_media() {
    DIRECTORY="/run/media/$USER"

    if [ -d "$DIRECTORY" ]
    then
        [ "$(/usr/bin/ls -A "$DIRECTORY")" ] && echo "+@fg=3;禍" || echo "+@fg=4;禍"
    else
        echo "+@fg=4;禍"
    fi
}

_bluetooth() {
    if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]
    then
        echo "+@fg=4;"
    else
        if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]
        then
            echo "+@fg=2;"
        else
            echo "+@fg=3;"
        fi
    fi
}

_network() {
    $HOME/.local/bin/info/ssid
}

_mpd() {
    $HOME/.local/bin/info/mpdsong
}

while true; do
    echo "+@bg=0; $(_bluetooth) $(_media) +@fg=4;$(_ibusstate)+|R+@fg=0; +@bg=8;   +@fn=1;$(_mpd)+@fn=0; +@bg=0; +@bg=6; $(_battery) +@bg=0; +@bg=5; $(_pulseaudio) +@bg=0; +@bg=2; $(_cpu)| $(_tempintel) +@bg=0; +@bg=4; $(_ram) +@bg=0; +@bg=3; $(_network)|$(_protonvpn) +@bg=0;"
    sleep 1
done
