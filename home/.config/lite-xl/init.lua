-- put user settings here
-- this module will be loaded after everything else when the application starts
-- it will be automatically reloaded when saved

local core = require "core"
local keymap = require "core.keymap"
local config = require "core.config"
local style = require "core.style"

------------------------------ Themes ----------------------------------------

core.reload_module("colors.nord")

--------------------------- Key bindings -------------------------------------

keymap.add { ["ctrl+q"] = "core:quit" }

------------------------------- Fonts ----------------------------------------

-- customize fonts:
style.font = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-regular.ttf", 14 * SCALE)
style.big_font = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-regular.ttf", 16 * SCALE)
style.code_font = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-regular.ttf", 14 * SCALE)
style.icon_font = renderer.font.load("/usr/share/lite-xl/fonts/icons.ttf", 18 * SCALE)
style.icon_big_font = renderer.font.load("/usr/share/lite-xl/fonts/icons.ttf", 24 * SCALE)

local italic = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-ss14-italic.ttf", 14 * SCALE)
style.syntax_fonts["comment"] = italic

local bolditalic = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-ss14-bolditalic.ttf", 14 * SCALE)
style.syntax_fonts["keyword"] = bolditalic
style.syntax_fonts["keyword2"] = bolditalic

local bold = renderer.font.load(HOME .. "/.config/lite-xl/fonts/iosevka-ss14-bold.ttf", 14 * SCALE)
style.syntax_fonts["function"] = bold
--
-- font names used by lite:
-- style.font          : user interface
-- style.big_font      : big text in welcome screen
-- style.icon_font     : icons
-- style.icon_big_font : toolbar icons
-- style.code_font     : code
--
-- the function to load the font accept a 3rd optional argument like:
--
-- {antialiasing="grayscale", hinting="full"}
--
-- possible values are:
-- antialiasing: grayscale, subpixel
-- hinting: none, slight, full

------------------------------ Plugins ----------------------------------------

-- enable or disable plugin loading setting config entries:

-- enable trimwhitespace, otherwise it is disable by default:
-- config.trimwhitespace = true
--
-- disable detectindent, otherwise it is enabled by default
-- config.detectindent = false
