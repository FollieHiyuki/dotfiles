# _________________               
# ___    |__  /__(_)_____ ________
# __  /| |_  /__  /_  __ `/_  ___/
# _  ___ |  / _  / / /_/ /_(__  ) 
# /_/  |_/_/  /_/  \__,_/ /____/  
#
# remap
abbr x extract
abbr p pulsemixer
alias startx='startx $HOME/.config/X11/xinitrc'
alias fehwpp="feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/*"
alias ls='exa -lF --icons --sort=type'
alias la='exa -laF --icons --sort=type'
alias lt='exa --tree'
alias cp='cp -vir'
alias mv='mv -vi'
alias rm='rm -vr'
alias mkdir='mkdir -pv'
abbr no 'grep -viP'
alias yarn='yarn --use-yarnrc $HOME/.config/yarn/config'
abbr tmux 'TERM=screen-256color command tmux'
# colorizing
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias ncdu='ncdu --color=dark'
alias ip='ip -color=always'
# vim
abbr v nvim
abbr vi 'fzf | xargs -r nvim -o'
# emacs
alias doom='$HOME/.config/emacs-config/doom/bin/doom'
abbr e 'emacs -nw'
abbr em "emacsclient -c -a 'emacs'"
# rsync
abbr rsyncdir "rsync -avP --delete-after"
# youtube-dl
abbr yoump3 'youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail'
abbr youflac 'youtube-dl --extract-audio --audio-format flac'
abbr youlist 'youtube-dl --yes-playlist'
# nnn
if command -v nnn >/dev/null
  alias ncp="cat $XDG_CONFIG_HOME/nnn/.selection | tr '\0' '\n'"

  function n --wraps nnn --description 'support nnn quit and change directory'
    # Block nesting of nnn in subshells
    if test -n "$NNNLVL"
      if [ (expr $NNNLVL + 0) -ge 1 ]
        echo "nnn is already running"
        return
      end
    end

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "-x" as in:
    #    set NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    if test -n "$XDG_CONFIG_HOME"
      set -x NNN_TMPFILE "$XDG_CONFIG_HOME/nnn/.lastd"
    else
      set -x NNN_TMPFILE "$HOME/.config/nnn/.lastd"
    end

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn $argv

    if test -e $NNN_TMPFILE
      source $NNN_TMPFILE
      rm $NNN_TMPFILE
    end
  end

  function n2 -d "Start nnn in dual pane (tmux)"
    tmux new-session -d -s nnn -n nnn "nnn -Hc"
    tmux split-window -h "nnn -Hc"
    TERM=screen-256color command tmux attach -t nnn:nnn
  end
end

# Lazy cd-ing
function ..    ; cd .. ; end
function ...   ; cd ../.. ; end
function ....  ; cd ../../.. ; end

# Bad hands
abbr gti git
abbr ragner ranger
abbr claer clear
abbr sduo sudo
abbr duso sudo
abbr daso doas
abbr daos doas
