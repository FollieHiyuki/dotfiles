# -------------- Nord ----------------- #
set -g fish_color_autosuggestion 4c566a
set -g fish_color_cancel -r
set -g fish_color_command 81a1c1
set -g fish_color_comment 434c5e
set -g fish_color_cwd green
set -g fish_color_cwd_root red
set -g fish_color_end 88c0d0
set -g fish_color_error ebcb8b
set -g fish_color_escape 00a6b2
set -g fish_color_history_current --bold
set -g fish_color_host normal
set -g fish_color_host_remote yellow
set -g fish_color_match --background=brblue
set -g fish_color_normal normal
set -g fish_color_operator 00a6b2
set -g fish_color_param eceff4
set -g fish_color_quote a3be8c
set -g fish_color_redirection b48ead
set -g fish_color_search_match 'bryellow' '--background=brblack'
set -g fish_color_selection 'white' '--bold' '--background=brblack'
set -g fish_color_status red
set -g fish_color_user brgreen
set -g fish_color_valid_path --underline

# -------------- OneDark ----------------- #
# set -g fish_color_autosuggestion 4b5263
# set -g fish_color_cancel -r
# set -g fish_color_command 61afef
# set -g fish_color_comment 3e4452
# set -g fish_color_cwd green
# set -g fish_color_cwd_root red
# set -g fish_color_end 56b6c2
# set -g fish_color_error e5c07b
# set -g fish_color_escape 00a6b2
# set -g fish_color_history_current --bold
# set -g fish_color_host normal
# set -g fish_color_host_remote yellow
# set -g fish_color_match --background=brblue
# set -g fish_color_normal normal
# set -g fish_color_operator 00a6b2
# set -g fish_color_param dfdfdf
# set -g fish_color_quote 98c379
# set -g fish_color_redirection c678dd
# set -g fish_color_search_match 'bryellow' '--background=brblack'
# set -g fish_color_selection 'white' '--bold' '--background=brblack'
# set -g fish_color_status red
# set -g fish_color_user brgreen
# set -g fish_color_valid_path --underline
