# __________
# ___  ____/_________   __
# __  __/  __  __ \_ | / /
# _  /___  _  / / /_ |/ /
# /_____/  /_/ /_/_____/

# cursor shapes
set -g fish_cursor_default block
set -g fish_cursor_insert line
set -g fish_cursor_replace_one underscore
set -g fish_cursor_visual block
set -g fish_vi_force_cursor

# no greeting
# set -g fish_greeting

# XDG thingy
set -gx XDG_CONFIG_HOME $HOME/.config
set -gx XDG_CACHE_HOME $HOME/.cache
set -gx XDG_DATA_HOME $HOME/.local/share
set -gx XDG_DATA_DIRS $HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share
if not command -v loginctl >/dev/null
  set -gx XDG_RUNTIME_DIR /tmp/fish.$USER
end
# alternate paths
set -gx LESSHISTFILE -
set -gx MOST_INITFILE $XDG_CONFIG_HOME/mostrc
set -gx TERMINFO $XDG_DATA_HOME/terminfo
set -gx TERMINFO_DIRS $XDG_DATA_HOME/terminfo:/usr/share/terminfo
set -gx GOPATH $XDG_DATA_HOME/go
set -gx GRADLE_USER_HOME $XDG_DATA_HOME/gradle
set -gx _JAVA_OPTIONS -Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java
set -gx NODE_REPL_HISTORY $XDG_CACHE_HOME/node_repl_history
set -gx NPM_CONFIG_USERCONFIG $XDG_CONFIG_HOME/npm/npmrc
set -gx NPM_CONFIG_PREFIX $XDG_DATA_HOME/npm-global
set -gx CARGO_HOME $XDG_DATA_HOME/cargo
set -gx RUSTUP_HOME $XDG_DATA_HOME/rustup
set -gx BUNDLE_USER_CONFIG $XDG_CONFIG_HOME/bundle
set -gx BUNDLE_USER_CACHE $XDG_CACHE_HOME/bundle
set -gx BUNDLE_USER_PLUGIN $XDG_DATA_HOME/bundle
set -gx GEM_HOME $XDG_DATA_HOME/gem
set -gx GEM_SPEC_CACHE $XDG_CACHE_HOME/gem
set -gx DOCKER_CONFIG $XDG_CONFIG_HOME/docker
set -gx GNUPGHOME $XDG_DATA_HOME/gnupg
set -gx IPYTHONDIR $XDG_CONFIG_HOME/ipython
set -gx JUPYTER_CONFIG_DIR $XDG_CONFIG_HOME/jupyter
set -gx XAUTHORITY $XDG_RUNTIME_DIR/Xauthority
set -gx XINITRC $XDG_CONFIG_HOME/X11/xinitrc
set -gx XSERVERRC $XDG_CONFIG_HOME/X11/xserverrc
set -gx WGETRC $XDG_CONFIG_HOME/wget/wgetrc
set -gx WEECHAT_HOME $XDG_CONFIG_HOME/weechat
set -gx RIPGREP_CONFIG_PATH $XDG_CONFIG_HOME/ripgrep/config

# env
set -gx EDITOR nvim
set -gx VISUAL nvim
set -gx PAGER "less -R"
set -gx SVDIR $HOME/.local/share/service
set -gx _JAVA_AWT_WM_NONREPARENTING 1
set -gx GPG_TTY (tty)
set -gx GOPROXY direct
# fzf
set -gx FZF_DEFAULT_OPTS "--multi --layout=reverse --inline-info
 --bind alt-j:preview-down,alt-k:preview-up
 --color fg:#D8DEE9,bg:#2E3440,hl:#A3BE8C,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C
 --color pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#81A1C1,marker:#EBCB8B"
# set -gx FZF_DEFAULT_OPTS "--multi --layout=reverse --inline-info
#  --bind alt-j:preview-down,alt-k:preview-up
#  --color fg:-1,bg:-1,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe
#  --color info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef"
set -gx FZF_DEFAULT_COMMAND "fd --type f --follow --hidden --exclude .git"
set -gx FZF_CTRL_T_OPTS "--no-height --preview-window 'left:60%' --preview '$HOME/.local/bin/garbage/preview {} 2>/dev/null'"
set -gx FZF_CTRL_T_COMMAND "fd . \$dir --follow --hidden --exclude .git"
set -gx FZF_ALT_C_OPTS "--preview 'exa -1a --sort=type --color always --icons {} 2>/dev/null'"
set -gx FZF_ALT_C_COMMAND "fd --type d --follow --hidden --exclude .git"
set -gx FZF_TMUX 1
# .NET
set -gx DOTNET_CLI_TELEMETRY_OPTOUT 1
# nodenv
set -gx NODENV_ROOT $XDG_DATA_HOME/nodenv
# pyenv
set -gx PYENV_ROOT $XDG_DATA_HOME/pyenv
# poetry
set -gx POETRY_HOME $XDG_DATA_HOME/poetry
# zoxide
set -gx _ZO_DATA_DIR $HOME/.local/share/zoxide
set -gx _ZO_ECHO 1
set -gx _ZO_FZF_OPTS "$FZF_DEFAULT_OPTS --no-multi"
set -gx _ZO_RESOLVE_SYMLINKS 1
# forgit
if test -n "$XDG_SESSION_TYPE"
  if test $XDG_SESSION_TYPE = "wayland"
    set -gx FORGIT_COPY_CMD wl-copy
  else
    if command -v xclip >/dev/null
      set -gx FORGIT_COPY_CMD "xclip -selection clipboard"
    else
      set -gx FORGIT_COPY_CMD "xsel -i -b"
    end
  end
end
# cht.sh
set -gx CHTSH "$XDG_CONFIG_HOME/cht.sh"
# nnn
if command -v nnn >/dev/null
  set -gx TERMINAL alacritty
  set -gx NNN_OPTS HUedx
  set -gx NNN_PLUG 'b:bulknew;c:fzcd;o:fzopen;z:fzz;d:gpgd;e:gpge;h:hexview;m:nmount;n:nuke;t:preview-tui;r:renamer;p:rsynccp;j:splitjoin;s:suedit;i:ringtone'
  set -gx NNN_FIFO /tmp/nnn.fifo
  set -gx NNN_ARCHIVE '\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
  # set -gx NNN_OPENER $XDG_CONFIG_HOME/nnn/plugins/nuke
  # set -gx NNN_TRASH=1
  set -gx USE_SCOPE 1
  set -gx NNN_FCOLORS 'c1e20402006006f7c6d6ab01'
  set -gx NNN_COLORS '2345'
  set -gx NNN_BMS "h:~;C:~/Code;D:~/Downloads;P:~/Pictures;V:~/Videos;A:~/Media;U:~/Music;f:~/.config;l:~/.local/share;e:/etc;u:/usr/share;o:/opt;b:/boot;m:/media;M:/mnt;i:/run/media/$USER;v:/var;t:/tmp;d:/dev;s:/srv;r:/"
  set -gx NNN_SSHFS "sshfs -o reconnect,idmap=user,follow_symlinks"
end
# lf
if command -v lf >/dev/null
  set -gx LF_ICONS "\
  tw=:\
  st=:\
  ow=:\
  dt=:\
  di=:\
  fi=:\
  ln=:\
  or=:\
  ex=:\
  *.7z=:\
  *.a=:\
  *.ace=:\
  *.alz=:\
  *.ai=:\
  *.apk=:\
  *.arc=:\
  *.arj=:\
  *.asm=:\
  *.asp=:\
  *.aup=:\
  *.avi=:\
  *.awk=:\
  *.bash=:\
  *.bat=:\
  *.bmp=:\
  *.bz=:\
  *.bz2=:\
  *.c=:\
  *.c++=:\
  *.cab=:\
  *.cbr=:\
  *.cbz=:\
  *.cc=:\
  *.class=:\
  *.clj=:\
  *.cljc=:\
  *.cljs=:\
  *.cmake=:\
  *.cmd=:\
  *.coffee=:\
  *.conf=:\
  *.cp=:\
  *.cpio=:\
  *.cpp=:\
  *.cs=:\
  *.csh=:\
  *.css=:\
  *.cue=:\
  *.cvs=:\
  *.cxx=:\
  *.d=:\
  *.dart=:\
  *.db=:\
  *.deb=:\
  *.diff=:\
  *.dll=:\
  *.doc=:\
  *.docx=:\
  *.dump=:\
  *.dwm=:\
  *.dz=:\
  *.edn=:\
  *.eex=:\
  *.efi=:\
  *.ejs=:\
  *.elf=:\
  *.elm=:\
  *.epub=:\
  *.ear=:\
  *.erl=:\
  *.esd=:\
  *.ex=:\
  *.exe=:\
  *.exs=:\
  *.f#=:\
  *.fifo=|:\
  *.fish=:\
  *.flac=:\
  *.flv=:\
  *.fs=:\
  *.fsi=:\
  *.fsscript=:\
  *.fsx=:\
  *.gem=:\
  *.gemspec=:\
  *.gif=:\
  *.go=:\
  *.gz=:\
  *.gzip=:\
  *.h=:\
  *.haml=:\
  *.hbs=:\
  *.hh=:\
  *.hpp=:\
  *.hrl=:\
  *.hs=:\
  *.htaccess=:\
  *.htm=:\
  *.html=:\
  *.htpasswd=:\
  *.hxx=:\
  *.ico=:\
  *.img=:\
  *.ini=:\
  *.iso=:\
  *.jar=:\
  *.java=:\
  *.jl=:\
  *.jpeg=:\
  *.jpg=:\
  *.js=:\
  *.json=:\
  *.jsx=:\
  *.key=:\
  *.ksh=:\
  *.leex=:\
  *.less=:\
  *.lha=:\
  *.lhs=:\
  *.log=:\
  *.lrz=:\
  *.lua=:\
  *.lz=:\
  *.lz4=:\
  *.lzh=:\
  *.lzma=:\
  *.lzo=:\
  *.m2v=:\
  *.m4a=:\
  *.m4v=:\
  *.markdown=:\
  *.md=:\
  *.mdx=:\
  *.mjpeg=:\
  *.mjpg=:\
  *.mjs=:\
  *.mkv=:\
  *.ml=λ:\
  *.mli=λ:\
  *.mng=:\
  *.mov=:\
  *.mp3=:\
  *.mp4=:\
  *.mp4v=:\
  *.mpeg=:\
  *.mpg=:\
  *.msi=:\
  *.mustache=:\
  *.nix=:\
  *.o=:\
  *.odt=:\
  *.ods=:\
  *.odp=:\
  *.ogg=:\
  *.pdf=:\
  *.php=:\
  *.pl=:\
  *.pm=:\
  *.png=:\
  *.pp=:\
  *.ppt=:\
  *.pptx=:\
  *.pro=:\
  *.ps1=:\
  *.psb=:\
  *.pub=:\
  *.py=:\
  *.pyc=:\
  *.pyd=:\
  *.pyo=:\
  *.r=ﳒ:\
  *.rake=:\
  *.rar=:\
  *.rb=:\
  *.rc=:\
  *.rlib=:\
  *.rmd=:\
  *.rom=:\
  *.rpm=:\
  *.rproj=鉶:\
  *.rs=:\
  *.rss=:\
  *.rtf=:\
  *.rz=:\
  *.s=:\
  *.sar=:\
  *.sass=:\
  *.scala=:\
  *.scss=:\
  *.sh=:\
  *.slim=:\
  *.sln=:\
  *.so=:\
  *.sql=:\
  *.styl=:\
  *.suo=:\
  *.svg=:\
  *.swift=:\
  *.swm=:\
  *.t=:\
  *.t7z=:\
  *.tar=:\
  *.taz=:\
  *.tbz=:\
  *.tbz2=:\
  *.tex=ﭨ:\
  *.tgz=:\
  *.tif=:\
  *.tiff=:\
  *.tlz=:\
  *.toml=:\
  *.ts=:\
  *.tsx=:\
  *.txz=:\
  *.tz=:\
  *.tzo=:\
  *.tzst=:\
  *.twig=:\
  *.vifm=:\
  *.vim=:\
  *.vimrc=:\
  *.vob=:\
  *.vue=﵂:\
  *.wav=:\
  *.war=:\
  *.webm=:\
  *.webmanifest=:\
  *.webp=:\
  *.wim=:\
  *.xbm=:\
  *.xbps=:\
  *.xcplayground=:\
  *.xhtml=:\
  *.xls=:\
  *.xlsx=:\
  *.xml=:\
  *.xpm=:\
  *.xul=:\
  *.xz=:\
  *.yaml=:\
  *.yml=:\
  *.z=:\
  *.zip=:\
  *.zoo=:\
  *.zsh=:\
  *.zip=:\
  *.zst=:\
  "
end

# PATH
set -gx fish_user_paths $HOME/.local/bin $HOME/.local/bin/fzf $NODENV_ROOT/bin $PYENV_ROOT/bin $POETRY_HOME/bin $CARGO_HOME/bin $GOPATH/bin $NPM_CONFIG_PREFIX/bin
