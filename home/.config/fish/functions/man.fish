function man -d "man with colors"
    if test -d $HOME/.local/share/man
        set -x MANPATH "$HOME/.local/share/man:$__fish_data_dir/man:$MANPATH"
    else
        set -x MANPATH "$__fish_data_dir/man:$MANPATH"
    end

    set -x LESS_TERMCAP_mb (printf "\e[01;31m")    # begin blink
    set -x LESS_TERMCAP_md (printf "\e[01;36m")    # begin bold
    set -x LESS_TERMCAP_me (printf "\e[0m")        # reset bold/blink
    set -x LESS_TERMCAP_se (printf "\e[0m")        # reset reverse video
    set -x LESS_TERMCAP_so (printf "\e[01;44;33m") # begin reverse video
    set -x LESS_TERMCAP_ue (printf "\e[0m")        # reset underline
    set -x LESS_TERMCAP_us (printf "\e[01;32m")    # begin underline
    command man $argv
end
