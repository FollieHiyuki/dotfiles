function tt -d "typing speed testing"
    iconv -f UTF-8 -t ASCII "$argv" | sed 's/--/-/g' \
        | sed '/^$/d' | sed 's/^[ \t]*//' | sed 's/\(^.\{1,80\}\).*/\1/' \
        > /tmp/tt.txt
    command tt /tmp/tt.txt
end
