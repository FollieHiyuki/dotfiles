#!/bin/sh

MUSIC_DIR=$HOME/Music/
COVER=/tmp/cover.jpg

if command -v mako >/dev/null
then
	notification=mako
else
  notification=dunst
fi

{
	file="$(mpc --format %file% current -p 6600)"
	album_dir="${file%/*}"
	if [ -z "$album_dir" ]; then exit 1; fi
	album_dir="$MUSIC_DIR/$album_dir"
	covers="$(find "$album_dir" -type d -exec find {} -maxdepth 1 -type f -iregex ".*/.*\(Album\|album\|Cover\|cover\|Folder\|folder\|Artwork\|artwork\|Front\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)" \; )"
	src="$(echo "$covers" 2>/dev/null | tr -d '\n' | head -n1)"
	rm -f "$COVER" 

	# For Notifications
	if [ -n "$src" ] ; then
		# Resize the image's width to 64px
		convert "$src" -resize 64x "$COVER"
		if [ -f "$COVER" ] ; then
			notify-send -u low -i ${COVER} "  Now Playing" "$(mpc --format '%title% \n%artist% - %album%' current)"
		fi
	else
		notify-send -u low -i $HOME/.config/${notification}/headphones.png "  Now Playing" "$(mpc --format '%title% \n%artist% - %album%' current)"
	fi
} &
