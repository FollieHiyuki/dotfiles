#!/bin/sh

# Startup applications
pipewire &
dunst &
sxhkd &
xsetroot -cursor_name left_ptr &
$HOME/.config/polybar/run_polybar.sh &
ibus-daemon -drx &
greenclip daemon &
# emacs --daemon &
# $HOME/.local/bin/X11/monitors.sh
# animatedwall --full ~/Pictures/Animated/fubuki.mp4 &
feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/* &
mpDris2 &

# Workspaces
bspc monitor eDP-1 -d 1 2 3 4 5 6 7 8
bspc monitor HDMI-2 -d 9 10

# Modes
bspc config top_padding					    0
bspc config bottom_padding				  0
bspc config left_padding				    0
bspc config right_padding				    0
bspc config border_width        		2
bspc config window_gap          		10

bspc config split_ratio         		  0.52
bspc config borderless_monocle  		  false
bspc config gapless_monocle     		  false
bspc config single_monocle	 			    true
bspc config click_to_focus	 			    none
bspc config focus_follows_pointer		  true

bspc config pointer_modifier			    super
bspc config pointer_action1				    move
bspc config pointer_action2				    resize_side
bspc config pointer_action3				    resize_corner

bspc config remove_disabled_monitors	    true
bspc config remove_unplugged_monitors	    true
bspc config merge_overlapping_monitors	  true

# --- Nord --- #
bspc config normal_border_color			    "#2e3440"
bspc config active_border_color			    "#ebcb8b"
bspc config focused_border_color		    "#81a1c1"
bspc config presel_feedback_color		    "#d8dee9"
# -- OneDark -- #
# bspc config normal_border_color			 "#282c34"
# bspc config active_border_color			 "#e5c07b"
# bspc config focused_border_color		 "#61afef"
# bspc config presel_feedback_color		 "#abb2bf"

# Program rules
bspc rule -a "Thunderbird"        desktop='^3' follow=on
bspc rule -a "mpv"						    desktop='^6' state=tiled follow=on
bspc rule -a "Zathura"					  desktop='^4' follow=on
bspc rule -a "Element"						desktop='^7' follow=on
bspc rule -a "Emacs" 					    state=tiled
bspc rule -a "Komikku"            state=floating follow=on
