#!/bin/sh

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]
then
	echo ""
else
	if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]
	then
		echo "%{F#bf616a}"
    else
        echo "%{F#81a1c1}"
    fi
fi
