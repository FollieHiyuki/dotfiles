#!/bin/sh

if [ $(bluetoothctl show | grep "Powered: yes" | wc -c) -eq 0 ]
then
	echo ""
else
	if [ $(echo info | bluetoothctl | grep 'Device' | wc -c) -eq 0 ]
	then
		echo "%{F#e5c07b}"
    else
        echo "%{F#61afef}"
    fi
fi
