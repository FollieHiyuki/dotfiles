;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Some functionality uses this to identify you
(setq user-full-name "FollieHiyuki"
      user-mail-address "folliekazetani@protonmail.com")

;; Set firefox as the default browser
(setq browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "firefox")

;; Secrets
(setq auth-sources '("~/.config/doom/.authinfo.gpg"))

;; Better default settings
(setq-default
 ;; Tab vs space
 indent-tabs-mode nil
 tab-width 4
 evil-shift-width 4
 standard-indent 4
 line-spacing 2
 ;; From @tecosaur
 delete-by-moving-to-trash t
 window-combination-resize t
 x-stretch-cursor t)
(setq undo-limit 80000000
      evil-want-fine-undo t
      auto-save-default t
      truncate-string-ellipsis "…")
(global-subword-mode 1) ;; Use CamelCase words

;; Font settings
(setq doom-font (font-spec :family "Iosevka" :size 16 :weight 'semi-light)
      doom-serif-font (font-spec :family "Iosevka Slab" :size 16 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 16)
      doom-big-font (font-spec :family "Iosevka" :size 24 :weight 'semi-light))
(after! unicode-fonts
  (setq doom-unicode-font doom-font)
  (dolist (unicode-block '("CJK Compatibility"
                           "CJK Compatibility Forms"
                           "CJK Compatibility Ideographs"
                           "CJK Compatibility Ideographs Supplement"
                           "CJK Radicals Supplement"
                           "CJK Strokes"
                           "CJK Symbols and Punctuation"
                           "CJK Unified Ideographs"
                           "CJK Unified Ideographs Extension A"
                           "CJK Unified Ideographs Extension B"
                           "CJK Unified Ideographs Extension C"
                           "CJK Unified Ideographs Extension D"
                           "CJK Unified Ideographs Extension E"))
      (push "Sarasa Mono J" (cadr (assoc unicode-block unicode-fonts-block-font-mapping)))))
;;(setq inhibit-compacting-font-caches t)
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :weight bold))

;; Japanese input method
(use-package! mozc
  :config
  (setq default-input-method "japanese-mozc"))
;; (use-package! mozc-cand-posframe
;;   :after mozc
;;   :config
;;   (setq mozc-candidate-style 'posframe)
;;   (custom-set-faces!
;;     '(mozc-cand-posframe-normal-face :inherit tooltip)
;;     '(mozc-cand-posframe-focused-face :inherit highlight)
;;     '(mozc-cand-posframe-footer-face :inherit default)))

;; Work around modeline cut off. See hlissner/doom-emacs#2967
(setq all-the-icons-scale-factor 1.0)
(after! doom-modeline
  (custom-set-faces!
    '(mode-line :height 0.98)
    '(mode-line-inactive :height 0.98))
  (setq doom-modeline-major-mode-icon t
        doom-modeline-major-mode-color-icon t
        doom-modeline-buffer-encoding t
        doom-modeline-enable-word-count t
        doom-modeline-unicode-fallback t))
(custom-set-faces!
  '(doom-modeline-buffer-modified :foreground "orange"))

;; Window split position
(setq evil-vsplit-window-right t
      evil-split-window-below t)

;; Emoji
(after! emojify
  (setq emojify-display-style 'unicode))

;; Theme
(setq doom-theme 'doom-nord)
(setq doom-themes-enable-bold t
      doom-themes-enable-italic t)
(after! treemacs
  (setq doom-themes-treemacs-theme 'doom-colors
        doom-themes-treemacs-enable-variable-pitch nil))

;; Pick an image for dashboard
;;(defun random-choice (items)
;;      (let* ((size (length items))
;;            (index (random size)))
;;      (nth index items)))
;;(defvar *kawaii* (random-choice (delete "."
;;      (delete ".." (directory-files (expand-file-name "images" doom-private-dir))))))
(setq +doom-dashboard-banner-file "kokkoro.png"
      +doom-dashboard-banner-dir (expand-file-name "images" doom-private-dir)
      +doom-dashboard-banner-padding '(0 . 2)
      +doom-dashboard-functions '(doom-dashboard-widget-banner
                                  doom-dashboard-widget-shortmenu
                                  doom-dashboard-widget-loaded))

;; Line number
(setq display-line-numbers-type 'relative)
(setq global-hl-line-modes nil)

;; Git-gutter
(after! git-gutter
  (setq +vc-gutter-default-style nil))

;; Indent
(after! highlight-indent-guides
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-responsive t))

;; Use ranger.el instead of default dired
(after! ranger
  (setq ranger-parent-depth 0
        ranger-cleanup-eagerly t
        ranger-show-hidden t
        ranger-max-preview-size 20
        ranger-dont-show-binary t))

;; Projectile
(after! projectile
  (setq projectile-project-search-path '("~/Code/")
        projectile-auto-discover nil))

;; Ivy / Counsel
(setq +ivy-buffer-preview t)
(after! counsel
  (setq counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only))
(map! "C-M-j" 'counsel-switch-buffer)
(map! :map minibuffer-local-map
      "C-r" 'counsel-minibuffer-history)
(use-package! all-the-icons-ivy-rich
  :defer-incrementally counsel-projectile
  :init (all-the-icons-ivy-rich-mode 1)
  :config
  (setq all-the-icons-ivy-rich-icon-size 1.0))
(use-package! ivy-rich
  :after all-the-icons-ivy-rich)

;; Company
(after! company
  ;;(add-hook 'evil-normal-state-entry-hook #'company-abort)
  (setq company-idle-delay 0.5
        company-minimum-prefix-length 2
        company-show-numbers nil))

;; lsp
(use-package! lsp-treemacs
  :after (treemacs lsp)
  :config
  (lsp-treemacs-sync-mode 1))
(setq lsp-clients-clangd-args '("-j=2"
                                "--background-index"
                                "--clang-tidy"
                                "--completion-style=detailed"
                                "--pch-storage=memory"
                                "--header-insertion=iwyu"
                                "--header-insertion-decorators"))
(after! lsp-clangd (set-lsp-priority! 'clangd 2))  ;; Prefer clangd instead of default ccls
;; Self-built lua-language-server in .local/etc/lsp/lua-language-server
(after! lsp-lua
  (setq lsp-clients-lua-language-server-bin (expand-file-name "lsp/lua-language-server/bin/Linux/lua-language-server" doom-etc-dir)
        lsp-clients-lua-language-server-main-location (expand-file-name "lsp/lua-language-server/main.lua" doom-etc-dir)
        lsp-lua-completion-call-snippet "Replace"
        lsp-lua-hint-enable t
        lsp-lua-runtime-path ["?.lua"
                              "?/init.lua"
                              "?/?.lua"
                              "/home/follie/.luarocks/share/lua/5.3/?.lua"
                              "/home/follie/.luarocks/share/lua/5.3/?/init.lua"
                              "/usr/share/lua/5.3/?/init.lua"
                              "/usr/share/5.3/?.lua"]
        lsp-lua-runtime-version "Lua 5.3"
        lsp-lua-telemetry-enable nil
        lsp-lua-workspace-library '((/home/follie/.luarocks/share/lua/5.3 . t) (/usr/share/lua/5.3 . t))))

;; Quicker which-key
(after! which-key
  (setq which-key-idle-delay 0.5))

;; Self-built parinfer-rust in .local/etc/parinfer-rust (cargo build --release --features emacs)
(use-package! parinfer-rust-mode
  :init
  (setq parinfer-rust-library
        (concat doom-etc-dir "parinfer-rust/target/release/libparinfer_rust.so")
        parinfer-rust-auto-download nil))

;; Org
(setq org-directory "~/Documents/Org/")
(after! org
  (setq org-default-notes-file "~/Documents/Org/notes.org"
        org-agenda-files '("~/Documents/Org/agenda/agenda.org" "~/Documents/Org/agenda/archive.org")
        org-confirm-babel-evaluate nil
        org-ellipsis "▾"
        org-log-done t
        org-log-into-drawser t
        org-insert-heading-respect-content nil
        org-hide-emphasis-markers t)
  (add-to-list 'org-modules 'ol-info)
  ;; Org-noter
  (setq org-noter-notes-search-path '("~/Documents/Org/noter/"))
  ;; Org-journal
  (setq org-journal-dir "~/Documents/Org/journal/"))
(defun follie/comfy-org-editing ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1)
  (display-line-numbers-mode -1))
(add-hook 'org-mode-hook 'follie/comfy-org-editing)
(add-hook! org-mode (electric-indent-local-mode -1))
(defun follie/org-toggle-emphasis-markers ()
  "Toggle emphasis markers in an Org buffer"
  (interactive)
  (if org-hide-emphasis-markers
      (setq org-hide-emphasis-markers nil)
    (setq org-hide-emphasis-markers t))
  (org-mode-restart))
(map! :map org-mode-map
      :localleader :desc "org-toggle-emphasis-markers"
      "z" #'follie/org-toggle-emphasis-markers)

;; Org-tree-slide
(defun follie/presentation-start ()
  (text-scale-set 2)
  (hide-mode-line-mode 1))
(defun follie/presentation-stop ()
  (text-scale-set 0)
  (hide-mode-line-mode -1))
(use-package! org-tree-slide
  :commands org-tree-slide-mode
  :config
  (setq org-tree-slide-skip-outline-level 0
        org-image-actual-width nil
        org-tree-slide-heading-emphasis t
        org-tree-slide-modeline-display nil
        org-tree-slide-header t
        org-tree-slide-slide-in-effect t
        org-tree-slide-breadcrumbs " ▶ "
        org-tree-slide-activate-message "Presentation starto..."
        org-tree-slide-deactivate-message "Thanks for listening!")
  (add-hook! 'org-tree-slide-mode-hook #'evil-normalize-keymaps)
  (add-hook! 'org-tree-slide-play-hook #'follie/presentation-start)
  (add-hook! 'org-tree-slide-stop-hook #'follie/presentation-stop))

;; Latex
(setq +latex-viewers '(pdf-tools))

;; Spell checker
(after! flyspell
  (setq flyspell-lazy-idle-seconds 2))

;; Magit
(after! magit
  (setq magit-diff-refine-hunk 'all))
;;(add-hook 'magit-mode-hook (lambda () (magit-delta-mode +1)))

;; Elfeed
(setq rmh-elfeed-org-files (list (expand-file-name "elfeed.org" doom-private-dir)))
;;(add-hook! 'elfeed-search-mode-hook 'elfeed-update)
(use-package! elfeed-goodies
  :after elfeed
  :config
  (elfeed-goodies/setup)
  (setq elfeed-goodies/entry-pane-size 0.5))

;; Elpher
(defun elpher--init-hook ()
  (setq visual-fill-column-center-text t)
  (visual-fill-column-mode 1))
(use-package! elpher
  :defer t
  :config
  (setq elpher-use-tls t
        elpher-auto-disengage-TLS t
        elpher-connection-timeout 10
        elpher-gemini-max-fill-width 80
        elpher-gemini-TLS-cert-checks nil)
  (add-hook 'elpher-mode-hook 'elpher--init-hook))

;; mu4e
(setq +mu4e-alert-bell-cmd nil
      +mu4e-mu4e-mail-path "~/Mail")
(use-package! mu4e
  :commands mu4e mu4e-compose-new
  :config
  (setq mu4e-update-interval (* 10 60)
        mu4e-view-image-max-width 600
        mu4e-context-policy 'always-ask
        mu4e-compose-context-policy 'always-ask
        mu4e-index-cleanup nil
        mu4e-index-lazy-check t))
(set-email-account! "Gmail"
  '((mu4e-sent-folder       . "/Gmail/[Gmail]/Sent Mail")
    (mu4e-drafts-folder     . "/Gmail/[Gmail]/Drafts")
    (mu4e-trash-folder      . "/Gmail/[Gmail]/Trash")
    (mu4e-refile-folder     . "/Gmail/[Gmail]/All Mail")
    (smtpmail-smtp-user     . "example@gmail.com")
    (smtpmail-smtp-server   . "smtp.gmail.com")
    (smtpmail-smtp-service  . 465)
    (smtpmail-stream-type   . ssl)
    (user-full-name         . "FGoogle")
    (user-mail-address      . "example@gmail.com")
    (mu4e-compose-signature . "---\nfgoogle"))
  t)

;; circe
(defun my-fetch-password (&rest params)
  (require 'auth-source)
  (let ((match (car (apply #'auth-source-search params))))
    (if match
        (let ((secret (plist-get match :secret)))
          (if (functionp secret)
              (funcall secret)
            secret))
      (error "Password not found for %S" params))))
(after! circe
  (set-irc-server! "irc.libera.chat"
    '(:tls t
      :port 6697
      :nick "FollieHiyuki"
      :sasl-username "FollieHiyuki"
      :sasl-password (my-fetch-password :user "FollieHiyuki" :host "irc.libera.chat")
      :channels ("#river", "#voidlinux", "#xbps", "artix", "#emacs", "#gentoo", "#guix", "##otaku"))))

;; Cheatsheet
;; (use-package! tldr
;;   :commands tldr tldr-mode
;;   :config
;;   (setq tldr-directory-path "~/.local/share/tldr"))

;; Keycast
(use-package! keycast
  :commands keycast-mode
  :config
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (progn
          (add-hook 'pre-command-hook 'keycast--update t)
          (add-to-list 'global-mode-string '("" mode-line-keycast " ")))
      (remove-hook 'pre-command-hook 'keycast--update)
      (setq global-mode-string (remove '("" mode-line-keycast " ") global-mode-string))))
  (custom-set-faces!
    '(keycast-command :inherit doom-modeline-debug
                      :height 0.9)
    '(keycast-key :inherit custom-modified
                  :height 1.1
                  :weight bold)))

;; To read big files
;; (use-package! vlf-setup
;;   :defer-incrementally vlf-tune vlf-base vlf-write vlf-search vlf-occur vlf-follow vlf-ediff vlf)

;; Nicer page break
;; (use-package! page-break-lines
;;   :commands page-break-lines-mode
;;   :init
;;   (autoload 'turn-on-page-break-lines-mode "page-break-lines"))

;; Like neofetch, but for eshell
(use-package! eshell-info-banner
  :defer t
  :after eshell
  :hook (eshell-banner-load . eshell-info-banner-update-banner))

;; (buffer-expose-mode 1)

;; xbps but with Emacs
;;(use-package! system-packages
;;  :defer t
;;  :config
;;  (setq system-packages-use-sudo t))

(use-package! symon
  :commands symon-mode
  :custom
  (symon-sparkline-type 'gridded)
  :config
  (symon-display))

;; playerctl status on modeline
;; (use-package! doom-modeline-now-playing
;;   :after doom-modeline
;;   :config
;;   (doom-modeline-now-playing-timer)
;;   ;; Enable almost everywhere
;;   (doom-modeline-def-modeline 'main
;;     '(bar workspace-name window-number modals matches buffer-info remote-host buffer-position word-count parrot selection-info now-playing)
;;     '(objed-state misc-info persp-name battery grip irc mu4e gnus github debug repl lsp minor-modes input-method indent-info buffer-encoding major-mode process vcs checker))
;;   (doom-modeline-def-modeline 'special
;;     '(bar window-number modals matches buffer-info buffer-position word-count parrot selection-info now-playing)
;;     '(objed-state misc-info battery irc-buffers debug minor-modes input-method indent-info buffer-encoding major-mode process))
;;   (doom-modeline-def-modeline 'project
;;     '(bar window-number buffer-default-directory now-playing)
;;     '(misc-info battery irc mu4e gnus github debug minor-modes input-method major-mode process))
;;   (doom-modeline-def-modeline 'vcs
;;     '(bar window-number modals matches buffer-info buffer-position parrot selection-info now-playing)
;;     '(misc-info battery irc mu4e gnus github debug minor-modes buffer-encoding major-mode process))
;;   (doom-modeline-def-modeline 'info
;;     '(bar window-number buffer-info info-nodes buffer-position parrot selection-info now-playing)
;;     '(misc-info buffer-encoding major-mode))
;;   (doom-modeline-def-modeline 'media
;;     '(bar window-number buffer-size buffer-info now-playing)
;;     '(misc-info media-info major-mode process vcs))
;;   (doom-modeline-def-modeline 'message
;;     '(bar window-number modals matches buffer-info-simple buffer-position word-count parrot selection-info now-playing)
;;     '(objed-state misc-info battery debug minor-modes input-method indent-info buffer-encoding major-mode))
;;   (doom-modeline-def-modeline 'pdf
;;     '(bar window-number matches buffer-info pdf-pages now-playing)
;;     '(misc-info major-mode process vcs))
;;   (doom-modeline-def-modeline 'org-src
;;     '(bar window-number modals matches buffer-info-simple buffer-position word-count parrot selection-info now-playing)
;;     '(objed-state misc-info debug lsp minor-modes input-method indent-info buffer-encoding major-mode process checker))
;;   (doom-modeline-def-modeline 'timemachine
;;     '(bar window-number matches git-timemachine buffer-position word-count parrot selection-info now-playing)
;;     '(misc-info minor-modes indent-info buffer-encoding major-mode)))

;; EMMS
(use-package! emms
 :defer t
 :init
 (setq emms-seek-seconds 10)
 :config
 (emms-playing-time -1)
 (emms-mode-line -1)
 ;; Play music with emms directly
 (setq emms-source-file-default-directory "~/Music/"
       emms-info-asynchronously 1
       emms-playlist-buffer-name "*EMMS-Music*"
       emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find
       emms-browser-covers 'emms-browser-cache-thumbnail-async)
 ;; Use mpd backend
 (setq emms-player-mpd-server-name "localhost"
       emms-player-mpd-server-port "6600"
       emms-player-mpd-music-directory "~/Music/")
 (add-to-list 'emms-player-list 'emms-player-mpd)
 (add-to-list 'emms-info-functions
              'emms-info-mpd
              'emms-info-exiftool)
 (map! :map emms-playlist-mode-map
       :localleader
       "d" #'emms-insert-directory))
(use-package! ivy-emms
 :after emms
 :config
 (setq ivy-emms-default-action #'ivy-emms-play-next)
 (setq ivy-emms-default-multi-action #'ivy-emms-play-next-multi))
;;(use-package! emms-mode-line-cycle
;;  :after emms
;;  :config
;;  (emms-playing-time 1)
;;  (emms-mode-line 1)
;;  (emms-mode-line-cycle 1))

;; Duck the web with ivy
(use-package! counsel-web
  :after counsel
  :config
  (setq counsel-web-engine 'duckduckgo)
  (setq counsel-web-search-action #'browse-url)
  (setq counsel-web-search-alternate-action #'browse-url-firefox)
  (setq counsel-web-search-dynamic-update nil)
  (exwm-input-set-key (kbd "s-d") #'counsel-web-search))

;; Start pinentry
(pinentry-start)
;; System monitor in minibuffer
(symon-mode)
;; Additional status info on modeline
;;(setq display-time-day-and-date t)
(display-time-mode 1)
(if (not (equal "Battery status not available" (battery)))
    (display-battery-mode 1))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
