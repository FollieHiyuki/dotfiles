import os

PHONE = ''
URL_VIEW = 'urlview'
KEEP_MEDIA = 7
FILE_PICKER_CMD = "nnn -p {file_path}"
# FILE_PICKER_CMD = "ranger --choosefile={file_path}"
DOWNLOAD_DIR = os.path.expanduser("~/Downloads/")
MAILCAP_FILE = os.path.expanduser("~/.config/tg/mailcap")
CHAT_FLAGS = {
    "online": "●",
    "pinned": "P",
    "muted": "M",
    "unread": "U",
    "unseen": "?",
    "secret": "🔒",
    "seen": "✓",
}
MSG_FLAGS = {
    "selected": "*",
    "forwarded": "F",
    "new": "N",
    "unseen": "U",
    "edited": "E",
    "pending": "...",
    "failed": "💩",
    "seen": "✓",
}
LOG_LEVEL = "DEBUG"
LOG_PATH = os.path.expanduser("~/.local/share/tg/")
VOICE_RECORD_CMD = "ffmpeg -f avfoundation -i ':0' -c:a libopus -b:a 32k {file_path}"
NOTIFY_CMD = "notify-send '{title}-{subtitle}' '{msg}' -i {icon_path}"
USERS_COLORS = tuple(range(2,16))
