local M = {}

local c = require('themes.onedark.colors')
local hi = require('util').highlight

function M.highlight_editor()
    hi('ModeMsg', c.fg, '', '', '')
end

return M
