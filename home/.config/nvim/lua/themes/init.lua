local M = {}

function M.set(theme)
    -- Reset everything
    vim.api.nvim_command('hi clear')
    if vim.fn.exists('syntax_on') then vim.api.nvim_command('syntax reset') end
    vim.opt.background = 'dark'

    -- Get theme specs
    local t = require('themes.' .. theme)
    vim.g.colors_name = theme

    -- Load highlight groups
    t.highlight_editor()
    t.highlight_syntax()
    t.set_vim_termcolors()
    t.highlight_plugins()
    t.highlight_languages()
    t.highlight_treesitter()
    t.highlight_lsp()
end

return M
