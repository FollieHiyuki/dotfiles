local api = vim.api
local wk = require('which-key')

local function pandoc_convert(ofiletype)
    if ofiletype == nil then
        ofiletype = 'html'
    end

    local ifilename = vim.fn.expand('%:p')
    local ofilename = vim.fn.expand('%:p:r') .. '.' .. ofiletype
    local cmd

    if ofiletype == 'pdf' then
        cmd = string.format('pandoc %s -o %s --pdf-engine=xelatex -V "mainfont:Iosevka Etoile" -V "sansfont:Iosevka Aile" -V "monofont:Iosevka" -V "geometry:margin=1in"', ifilename, ofilename)
    else
        cmd = string.format('pandoc %s -o %s', ifilename, ofilename)
    end
    vim.fn.jobstart(cmd)
end

-- Escape to normal mode in terminal buffer
api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', {noremap = true, silent = true})

-- Continuous indent
api.nvim_set_keymap('v', '<', '<gv', {noremap = true, silent = true})
api.nvim_set_keymap('v', '>', '>gv', {noremap = true, silent = true})

-- Also move up/down virual lines (:set wrap)
api.nvim_set_keymap('n', 'j', 'gj', {noremap = true, silent = true})
api.nvim_set_keymap('n', 'k', 'gk', {noremap = true, silent = true})

-- winshift.nvim
api.nvim_set_keymap('n', '<C-w><C-m>', ':WinShift<CR>', {noremap = true, silent = true})
api.nvim_set_keymap('n', '<C-w>m', ':WinShift<CR>', {noremap = true, silent = true})
api.nvim_set_keymap('n', '<C-A-h>', ':WinShift left<CR>', {noremap = true, silent = true})
api.nvim_set_keymap('n', '<C-A-j>', ':WinShift down<CR>', {noremap = true, silent = true})
api.nvim_set_keymap('n', '<C-A-k>', ':WinShift up<CR>', {noremap = true, silent = true})
api.nvim_set_keymap('n', '<C-A-l>', ':WinShift right<CR>', {noremap = true, silent = true})

-----------------
-- Normal mode --
-----------------
wk.register({
    -- Better Y
    Y = {'y$', 'Yank to eol'},

    -- Easier start and end of line
    H = {'^', 'Start of the line'},
    L = {'$', 'End of the line'},

    -- Close a window
    ['<C-q>'] = {'<C-w>q', 'Quit current window'},

    -- Close current buffer
    ['<A-q>'] = {':bdelete<CR>', 'Close current buffer'},

    -- Copy the whole buffer
    ['<A-a>'] = {':%y+<CR>', 'Copy whole buffer'},

    -- Remove trailing whitespace
    ['<A-w>'] = {':%s/\\s\\+$//e<CR>', 'Remove trailing'},

    -- Resize buffer
    ['<A-j>'] = {':resize -2<CR>', 'Resize vertical -2'},
    ['<A-k>'] = {':resize +2<CR>', 'Resize vertical +2'},
    ['<A-h>'] = {':vertical resize -2<CR>', 'Resize horizontal -2'},
    ['<A-l>'] = {':vertical resize +2<CR>', 'Resize horizontal +2'},

    -- Switch between tabs and spaces
    ['<A-t>'] = {
        function()
            if vim.opt.expandtab:get() then
                vim.opt.expandtab = false
                vim.opt.smarttab = false
                vim.opt.softtabstop = 0 -- reset to default
                vim.notify('Indent with Tabs.', vim.log.levels.INFO)
            else
                vim.opt.expandtab = true
                vim.opt.smarttab = true
                vim.opt.softtabstop = -1 -- fallback to shiftwidth
                vim.notify('Indent with Spaces.', vim.log.levels.INFO)
            end
        end,
        'Switch indent style'
    },

    -- Naming common keys
    ['['] = {
        name = 'Block motions (previous)',
        d = 'Previous diagnostics',
        g = 'Previous git hunk',
        m = 'Previous start of outer class',
        M = 'Previous end of outer class',
        ['['] = 'Previous start of outer function',
        [']'] = 'Previous end of outer function'
    },
    [']'] = {
        name = 'Block motions (next)',
        d = 'Next diagnostics',
        g = 'Next git hunk',
        m = 'Next start of outer class',
        M = 'Next end of outer class',
        ['['] = 'Next start of outer function',
        [']'] = 'Next end of outer function'
    },
    g = {
        name = 'Goto motions',
        c = {
            name = 'Comment',
            c = 'Current line'
        },
        d = 'Go to definition',
        D = 'Go to declaration',
        i = 'Go to implementation',
        n = {
            name = 'Incremental selection',
            i = 'Initialize selection'
        },
        r = 'Go to references',
        R = {':TroubleToggle lsp_references<CR>', 'Reference list'}
    },
    K = {name = 'Hover'},
    z = {name = 'Misc utils'},

    -- Window picker
    ['-'] = {':WindowPick<CR>', 'Choose window'},

    -- Move between tabs
    ['<TAB>'] = {':BufferLineCycleNext<CR>', 'Next buffer'},
    ['<S-TAB>'] = {':BufferLineCyclePrev<CR>', 'Previous buffer'},

    -- NvimTree
    ['<C-n>'] = {':NvimTreeToggle<CR>', 'NvimTree'},

    -- ToggleTerm
    ['<C-\\>'] = {':ToggleTerm<CR>', 'Toggle terminal'},
    ['<A-\\>'] = {':ToggleTerm direction=float<CR>', 'Toggle float terminal'},

    -- hop.nvim
    S = {':HopWord<CR>', 'Hop to word'},
    ['<C-s>'] = {':HopChar1<CR>', 'Hop to character'},
    ['<A-s>'] = {':HopPattern<CR>', 'Hop to pattern'}
})

-----------------
-- Visual mode --
-----------------
wk.register({
    a = {
        c = 'Outer class',
        f = 'Outer function'
    },

    i = {
        c = 'Inner class',
        f = 'Inner function'
    },

    g = {
        c = 'Comment',
        n = {
            name = 'Incremental selection',
            n = 'Increment node',
            s = 'Increment scope',
            m = 'Decrement node'
        }
    }
}, {mode = 'v'})

-----------------------------------
-- Normal mode (with leader key) --
-----------------------------------
wk.register({
    b = {
        name = 'Buffer',
        c = {':ColorizerToggle<CR>', 'Colorizer'},
        d = {
            name = 'Delete',
            a = {':BDelete all<CR>', 'All buffers'},
            d = {':BDelete this<CR>', 'Current buffer'},
            h = {':BDelete hidden<CR>', 'Hidden buffers'},
            n = {':BDelete nameless<CR>', 'Buffers without name'},
            o = {':BDelete other<CR>', 'Other buffers'}
        },
        j = {':BufferLineCyclePrev<CR>', 'Previous buffer'},
        k = {':BufferLineCycleNext<CR>', 'Next buffer'},
        n = {':enew<CR>', 'New buffer'},
        u = {':UndotreeToggle<CR>', 'Undotree'},
        v = {
            name = 'Convert',
            m = {function() pandoc_convert('md') end, 'To Markdown'},
            o = {function() pandoc_convert('org') end, 'To Org'},
            p = {function() pandoc_convert('pdf') end, 'To PDF'},
            r = {function() pandoc_convert('rst') end, 'To RST'},
            t = {function() pandoc_convert('tex') end, 'To LaTeX'},
            w = {function() pandoc_convert('html') end, 'To HTML'}
        }
    },

    d = {
        name = 'DAP',
        b = 'Toggle breakpoint',
        B = 'Set breakpoint with condition',
        c = 'Run to cursor',
        d = 'Disconnect adapter',
        e = 'Hover under cursor (UI)',
        f = 'Open float window (UI)',
        i = 'Step into function/method',
        l = 'List breakpoints',
        n = 'Resume the execution',
        o = 'Step out of function/method',
        r = 'Inspect in REPL',
        t = 'Toggle UI',
        v = 'Run again 1 step',
        z = 'Re-run the last adapter'
    },

    e = {
        name = 'Editor',
        a = {':EasyAlign<CR>', 'Align elements'},
        g = 'Generate annotations',
        h = {':TSHighlightCapturesUnderCursor<CR>', 'Syntax under cursor'},
        s = {':ISwapWith<CR>', 'Swap elements'},
        t = {':Twilight<CR>', 'Twilight mode'},
        v = {
            function()
                local venn_enabled = vim.inspect(vim.b.venn_enabled)
                if venn_enabled == 'nil' then
                    vim.b.venn_enabled = true
                    vim.api.nvim_command('setlocal virtualedit=all')
                    -- Draw lines with HJKL keystroke
                    vim.api.nvim_buf_set_keymap(0, 'n', 'H', '<C-v>h:VBox<CR>', {noremap = true, silent = true})
                    vim.api.nvim_buf_set_keymap(0, 'n', 'J', '<C-v>j:VBox<CR>', {noremap = true, silent = true})
                    vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<C-v>k:VBox<CR>', {noremap = true, silent = true})
                    vim.api.nvim_buf_set_keymap(0, 'n', 'L', '<C-v>l:VBox<CR>', {noremap = true, silent = true})
                    -- Draw boxes by pressing 'f' with visual selection
                    vim.api.nvim_buf_set_keymap(0, 'v', 'f', ':VBox<CR>', {noremap = true, silent = true})
                    vim.notify('Virtual box edit enabled.', vim.log.levels.INFO)
                else
                    vim.b.venn_enabled = nil
                    vim.api.nvim_command('setlocal virtualedit=block')
                    -- vim.api.nvim_command('mapclear <buffer>') -- quicker, but also deletes buf keymap for lsp
                    vim.api.nvim_buf_del_keymap(0, 'v', 'f')
                    vim.api.nvim_buf_del_keymap(0, 'n', 'H')
                    vim.api.nvim_buf_del_keymap(0, 'n', 'J')
                    vim.api.nvim_buf_del_keymap(0, 'n', 'L')
                    local present, _ = pcall(require, 'lspconfig') -- Check to re-enable buf keymap for hover
                    if present then
                        vim.api.nvim_buf_set_keymap(0, 'n', 'K', ':lua vim.lsp.buf.hover()<CR>', {noremap = true, silent = true})
                    else
                        vim.api.nvim_buf_del_keymap(0, 'n', 'K')
                    end
                    vim.notify('Virtual box edit disabled.', vim.log.levels.INFO)
                end
            end,
            'Toggle virtual box edit'
        },
        z = {':ZenMode<CR>', 'Zen mode'}
    },

    -- Telescope
    f = {
        name = 'Telescope',
        a = {':Telescope autocommands<CR>', 'Autocommands'},
        b = {':Telescope buffers<CR>', 'Buffers'},
        c = {':Telescope commands<CR>', 'Commands'},
        d = {':Telescope projects<CR>', 'Recent directories'},
        e = {':Telescope file_browser<CR>', 'File browser'},
        f = {':Telescope find_files<CR>', 'Find files'},
        g = {':Telescope live_grep<CR>', 'Live grep'},
        h = {':Telescope help_tags<CR>', 'Help tags'},
        i = {':Telescope highlights<CR>', 'Highlight groups'},
        j = {':Telescope symbols<CR>', 'Pick emojis'},
        k = {':Telescope keymaps<CR>', 'Normal keymaps'},
        m = {':Telescope marks<CR>', 'Bookmarks'},
        n = {':Telescope man_pages<CR>', 'Man pages'},
        o = {':Telescope oldfiles<CR>', 'Recent files'},
        p = {':Telescope project display_type=full<CR>', 'Projects'},
        r = {':Telescope reloader<CR>', 'Reload lua modules'},
        s = {':Telescope treesitter<CR>', 'Treesitter'},
        t = {':Telescope<CR>', 'Telescope'},
        u = {':Telescope current_buffer_fuzzy_find<CR>', 'Search current buffer'},
        v = {':Telescope vim_options<CR>', 'Vim options'},
        y = {':Telescope filetypes<CR>', 'Filetypes'},
        z = {':Telescope registers<CR>', 'Vim registers'}
    },

    -- Git
    g = {
        name = 'Git',
        b = 'Blame current line',
        p = 'Preview hunk',
        r = 'Reset hunk',
        R = 'Reset all hunks in buffer',
        s = 'Stage hunk',
        S = 'Stage buffer',
        u = 'Undo hunk',
        U = 'Reset buffer index',
        y = 'Get remote url for cursorline',
        Y = 'Get remote url',
        n = {':Neogit<CR>', 'Neogit'},
        f = {
            name = 'Telescope',
            a = {':Telescope git_stash<CR>', 'Stash'},
            b = {':Telescope git_bcommits<CR>', 'Buffer commits'},
            c = {':Telescope git_commits<CR>', 'Commits'},
            m = {':Telescope git_branches<CR>', 'Branches'},
            s = {':Telescope git_status<CR>', 'Status'}
        }
    },

    -- translate-shell.vim
    j = {
        name = 'Translate',
        t = {':Trans<CR>', 'Translate'},
        d = {':TransSelectDirection<CR>', 'Translate with direction'},
        r = {'cw<C-R>=system(\'trans -brief -no-ansi\', getreg(""))[:-2]<CR>', 'Translate and replace'},
        c = {'cw<C-R>=system(\'trans -brief -no-ansi :\', getreg(""))[:-2]<S-Left><S-Left><Right>', 'Translate and replace with direction'}
    },

    l = {
        name = 'LSP',
        a = 'Add workspace folder',
        d = 'Type definition',
        e = 'Line diagnostics',
        l = 'Set diagnostics loclist',
        n = 'Rename in buffer',
        o = 'Format buffer',
        r = 'Remove workspace folder',
        w = 'List workspace folders',
        x = 'Signature help',
        f = {
            name = 'Telescope',
            a = {':Telescope lsp_code_actions<CR>', 'Code actions'},
            d = {':Telescope lsp_document_diagnostics<CR>', 'Buffer diagnostics'},
            D = {':Telescope lsp_workspace_diagnostics<CR>', 'Workspace diagnostics'},
            e = {':Telescope lsp_dynamic_workspace_symbols<CR>', 'Dynamic workspace symbols'},
            i = {':Telescope lsp_implementations<CR>', 'Implementations'},
            n = {':Telescope lsp_definitions<CR>', 'Definitions'},
            r = {':Telescope lsp_references<CR>', 'References'},
            s = {':Telescope lsp_document_symbols<CR>', 'Buffer symbols'},
            S = {':Telescope lsp_workspace_symbols<CR>', 'Workspace symbols'}
        },
        g = {':SymbolsOutline<CR>', 'Symbol outline'},
        t = {
            name = 'Trouble',
            d = {':TroubleToggle lsp_definitions<CR>', 'Definition list'},
            e = {':TroubleToggle lsp_document_diagnostics<CR>', 'Document diagnostics list'},
            w = {':TroubleToggle lsp_workspace_diagnostics<CR>', 'Workspace diagnostics list'},
            l = {':TroubleToggle loclist<CR>', 'Location list items'},
            q = {':TroubleToggle quickfix<CR>', 'Quickfix list'},
            t = {':TroubleToggle<CR>', 'Summary'}
        }
    },

    p = {
        name = 'Find/Replace',
        o = 'Open spectre',
        p = 'Search in current file',
        w = 'Find/replace cursorword'
    },

    r = {
        name = 'REST',
        c = {'<Plug>RestNvim', 'Run request under cursor'},
        p = {'<Plug>RestNvimPreview', 'Preview request cURL command'},
        l = {'<Plug>RestNvimLast', 'Re-run last request'}
    },

    s = {
        name = 'Session',
        s = 'Save session for current directory',
        d = 'Stop automatically saving',
        l = 'Load last session',
        r = 'Load session for current directory'
    },

    -- t = {
    --     name = 'Todo',
    --     f = {':TodoQuickFix<CR>', 'Quick fix'},
    --     l = {':TodoLocList<CR>', 'Loclist'},
    --     t = {':TodoTrouble<CR>', 'Open in Trouble'},
    --     z = {':TodoTelescope<CR>', 'Open in Telescope'}
    -- },

    y ={
        name = 'Figlet',
        s = {':.!figlet -f standard<CR>', 'standard'},
        l = {':.!figlet -f slant<CR>', 'slant'},
        b = {':.!figlet -f banner<CR>', 'banner'},
        e = {':.!figlet -f lean<CR>', 'lean'},
        p = {':.!figlet -f speed<CR>', 'speed'},
        r = {':.!figlet -f roman<CR>', 'roman'},
        d = {':.!figlet -f doh<CR>', 'doh'},
        w = {':.!figlet -f starwars<CR>', 'starwars'},
        m = {':.!figlet -f morse<CR>', 'morse'},
        x = {':.!toilet -f term -F border<CR>', 'border'}
    }
}, {prefix = '<leader>'})

-----------------------------------
-- Visual mode (with leader key) --
-----------------------------------
wk.register({
    a = {
        name = 'Action',
        a = {':EasyAlign<CR>', 'Range align'}
    },

    d = {
        name = 'DAP',
        e = 'Hover on range (UI)'
    },

    g = {
        name = 'Git',
        r = 'Reset hunk',
        s = 'Stage hunk',
        y = 'Get remote url for range'
    },

    j = {
        name = 'Translate',
        t = {':Trans<CR>', 'Translate'},
        d = {':TransSelectDirection<CR>', 'Translate with direction'},
        r = {'c<C-R>=system(\'trans -brief -no-ansi\', getreg(""))[:-2]<CR>', 'Translate and replace'},
        c = {'c<C-R>=system(\'trans -brief -no-ansi :\', getreg(""))[:-2]<S-Left><S-Left><Right>', 'Translate and replace with direction'}
    },

    l = {
        name = 'LSP',
        f = {
            name = 'Telescope',
            a = {':Telescope lsp_range_code_actions<CR>', 'Range code actions'}
        },
        o = 'Range format'
    },

    p = 'Find/Replace'
}, {mode = 'v', prefix = '<leader>'})

------------------------
-- Filetype specified --
------------------------
vim.api.nvim_command [[
autocmd FileType org lua whichkeyOrg()
autocmd FileType markdown lua whichkeyMarkdown()
autocmd FileType html lua whichkeyHtml()
]]

_G.whichkeyOrg = function()
    wk.register({
        ['<leader>o'] = {
            name = 'Org',
            a = 'Agenda',
            A = 'Toggle ARCHIVE',
            c = 'Capture',
            e = 'Export',
            i = {
                name = 'Insert',
                h = 'Add headline',
                t = 'Add TODO heading and content',
                T = 'Add TODO heading',
            },
            J = 'Move subtree down',
            K = 'Move subtree up',
            o = 'Open at point',
            r = 'Refile',
            t = 'Set tags',
            ['$'] = 'Archive current headline'
        },
        ['<leader><CR>'] = 'Org meta return',
        ['<C-a>'] = 'Org increase date',
        ['<C-x>'] = 'Org decrease date',
        ['cid'] = 'Org change date',
        ['cit'] = 'Org TODO',
        ['ciT'] = 'Org TODO prev',
        ['<C-Space>'] = 'Org toggle checkbox',
        ['<TAB>'] = 'Org cycle folding',
        ['<S-TAB>'] = 'Org cycle global folding',
        ['<<'] = 'Org promote headline',
        ['>>'] = 'Org demote headline',
        ['<s'] = 'Org prmote subtree',
        ['>s'] = 'Org demote subtree',
        ['}'] = 'Org next visible heading',
        ['{'] = 'Org previous visible heading',
        [']]'] = 'Org forward heading',
        ['[['] = 'Org backward heading',
        ['g{'] = 'Org parent heading',
        ['?'] = 'Org help'
    })
end

_G.whichkeyMarkdown = function()
    wk.register({
        ['<leader>bp'] = {':MarkdownPreviewToggle<CR>', 'Preview markdown'}
    })
end

_G.whichkeyHtml = function()
    wk.register({
        ['<leader>bp'] = {':Bracey<CR>', 'Preview html'}
    })
end
