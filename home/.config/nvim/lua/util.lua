local M = {}

function M.highlight(group, guifg, guibg, attr, guisp)
    local fg = guifg ~= '' and 'guifg=' .. guifg or 'guifg=NONE'
    local bg = guibg ~= '' and 'guibg=' .. guibg or 'guibg=NONE'
    local style = attr ~= '' and 'gui=' .. attr or 'gui=NONE'
    local sp = guisp ~= '' and 'guisp=' .. guisp or ''

    local hl = 'hi ' .. group .. ' ' .. fg .. ' ' .. bg .. ' ' .. style .. ' ' .. sp
    vim.api.nvim_command(hl)
end

return M
