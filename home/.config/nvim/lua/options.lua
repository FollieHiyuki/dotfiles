local opt = vim.opt
local M = {}

function M.disable_default_plugins()
    vim.g.loaded_gzip              = 1
    vim.g.loaded_tar               = 1
    vim.g.loaded_tarPlugin         = 1
    vim.g.loaded_zip               = 1
    vim.g.loaded_zipPlugin         = 1
    vim.g.loaded_getscript         = 1
    vim.g.loaded_getscriptPlugin   = 1
    vim.g.loaded_vimball           = 1
    vim.g.loaded_vimballPlugin     = 1
    vim.g.loaded_matchit           = 1
    vim.g.loaded_matchparen        = 1
    vim.g.loaded_2html_plugin      = 1
    vim.g.loaded_logiPat           = 1
    vim.g.loaded_rrhelper          = 1
    vim.g.loaded_netrw             = 1
    vim.g.loaded_netrwPlugin       = 1
    vim.g.loaded_netrwSettings     = 1
    vim.g.loaded_netrwFileHandlers = 1
    vim.g.did_load_filetypes       = 1 -- use filetype.nvim instead
end

function M.load_options()
    -- This is the same as `:colorscheme nord/onedark`
    vim.g.colors_name = 'nord'
    -- Leader key
    vim.g.mapleader = ' '
    -- Python path
    -- vim.g.python_host_prog  = '/usr/bin/python'
    vim.g.python3_host_prog = '/usr/bin/python3'

    -- General settings
    opt.termguicolors  = true
    opt.mouse          = 'nv'
    opt.errorbells     = false
    opt.visualbell     = false
    -- opt.hidden         = true
    opt.fileformats    = 'unix'
    opt.magic          = true
    opt.virtualedit    = 'block'
    opt.encoding       = 'utf-8'
    opt.fileencoding   = 'utf-8'
    opt.clipboard      = 'unnamedplus'
    opt.wildignorecase = true
    opt.wildignore     = '.git,.hg,.svn,*.pyc,*.o,*.out,*.jpg,*.jpeg,*.png,*.gif,*.zip,**/tmp/**,*.DS_Store,**/node_modules/**'
    -- opt.history        = 10000
    -- opt.showmode       = false
    opt.jumpoptions    = 'stack'
    opt.formatoptions  = '1jcroql'
    opt.shortmess      = 'aoOTIcF'
    -- opt.startofline    = false
    opt.wrap           = false
    opt.sidescrolloff  = 4
    opt.scrolloff      = 4
    opt.whichwrap      = '<,>,[,],~'
    -- opt.ruler          = true
    -- opt.display        = 'lastline'
    -- opt.colorcolumn    = '80'
    -- opt.cursorline     = true
    -- opt.backspace      = 'indent,eol,start'
    opt.showcmd        = false
    opt.inccommand     = 'nosplit'
    -- opt.cmdheight      = 2
    -- opt.cmdwinheight   = 6
    opt.showtabline    = 2
    -- opt.laststatus     = 2
    -- opt.textwidth      = 80
    opt.synmaxcol      = 2500
    -- opt.shell          = 'bash'
    opt.grepformat     = '%f:%l:%c:%m'
    opt.grepprg        = 'rg --hidden --vimgrep --smart-case --'
    opt.diffopt        = 'filler,iwhite,internal,algorithm:patience'
    -- opt.spell          = true

    -- Transparency
    opt.pumblend      = 5
    opt.winblend      = 5

    -- Conceal
    opt.conceallevel   = 2
    opt.concealcursor  = 'niv'
    -- opt.foldenable     = true
    opt.foldlevelstart = 99

    -- Case insensitive
    opt.ignorecase     = true
    opt.smartcase      = true
    opt.infercase      = true

    -- Searching
    -- opt.incsearch      = true
    opt.hlsearch       = false
    -- opt.wrapscan    = true

    -- Update time
    -- opt.timeout        = true
    -- opt.ttimeout       = true
    opt.timeoutlen     = 500
    opt.ttimeoutlen    = 25
    opt.updatetime     = 150
    opt.redrawtime     = 1500

    -- No swapfile
    opt.backup         = false
    opt.writebackup    = false
    opt.swapfile       = false

    -- Completion menu
    -- opt.wildmenu       = true
    opt.wildmode       = 'full'
    opt.complete       = '.,w,b,k'
    opt.completeopt    = 'menu,menuone,noselect'
    opt.pumheight      = 16
    opt.helpheight     = 12
    opt.previewheight  = 12

    -- Window rules
    opt.splitbelow     = true
    opt.splitright     = true
    opt.switchbuf      = 'useopen'
    opt.winwidth       = 30
    opt.winminwidth    = 10
    opt.equalalways    = false

    -- Left column
    opt.signcolumn     = 'yes'
    opt.number         = true
    opt.relativenumber = true
    -- opt.numberwidth    = 6

    -- 4 spaces = 1 tab
    opt.shiftwidth     = 4
    opt.tabstop        = 4
    opt.softtabstop    = -1 -- fallback to 'shiftwidth'
    opt.smarttab       = true
    opt.expandtab      = true
    opt.smartindent    = true
    -- opt.autoindent     = true
    opt.shiftround     = true

    -- Trailings, line break
    opt.list           = true
    opt.listchars      = 'tab:»·,nbsp:+,trail:·,extends:→,precedes:←'
    opt.showbreak      = '↳  '
    -- opt.linebreak      = true
    -- opt.breakat        = [[\ \	;:,!?]]
    -- opt.breakindentopt = 'shift:4,min:20'

    -- Undo file path
    opt.undofile       = true
    -- opt.undodir        = vim.fn.stdpath('data') .. '/undo'
end

return M
