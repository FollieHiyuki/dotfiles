local async
async = vim.loop.new_async(vim.schedule_wrap(function()
    require('autocmd')
    require('plugins')
    require('mappings')
    async:close()
end))

local options = require('options')

options.disable_default_plugins()
options.load_options()
async:send()
