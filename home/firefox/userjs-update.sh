#!/bin/sh
# Use in conjunction with `firefox-sync` script
# (need to open Firefox at least once to generate a profile)

cur_dir="$PWD"

profile_dir=$(find ~/.var/app/org.mozilla.firefox/.mozilla/firefox/ -type d -name "static-*" | head -n 1)
profile=$(echo "${profile_dir}" | sed 's/^.*static-//g')

~/.local/bin/firefox-sync ${profile}

if [ ! -f "${profile_dir}/updater.sh" ]; then
    curl -fLo "${profile_dir}/updater.sh" https://raw.githubusercontent.com/arkenfox/user.js/master/updater.sh
    chmod 755 "${profile_dir}/updater.sh"
fi

cd ${profile_dir}
bash ./updater.sh
# To be sure both directories are synced
cp -f ./user.js ../${profile}/user.js
cp -f ./updater.sh ../${profile}/updater.sh

cd "${cur_dir}"
