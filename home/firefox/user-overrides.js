/* override recipe: these are some preferences that I like */

user_pref("ui.systemUsesDarkTheme", 1); // [FF67+] [HIDDEN PREF]
    // 0=light, 1=dark: with RFP this only affects chrome

user_pref("ui.prefersReducedMotion", 1); // [FF77+] [RESTART] [HIDDEN PREF]
    // disable chrome animations

user_pref("privacy.firstparty.isolate", false); // 4001
user_pref("network.cookie.cookieBehavior", 5); // 2701
user_pref("browser.contentblocking.category", "strict"); // 2701
    // ETP Strict mode (Total cookie protection + Smart block)
