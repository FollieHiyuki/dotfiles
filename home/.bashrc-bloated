# __________
# ___  ____/_________   __
# __  __/  __  __ \_ | / /
# _  /___  _  / / /_ |/ /
# /_____/  /_/ /_/_____/
#
# XDG thingy
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_DATA_DIRS=$HOME/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share
# alternate paths
export BASH_COMPLETION_USER_DIR="$XDG_DATA_HOME/bash"
export HISTFILE="$BASH_COMPLETION_USER_DIR/bash_history"
export LESSHISTFILE=-
export MOST_INITFILE=$XDG_CONFIG_HOME/mostrc
export TERMINFO="$XDG_DATA_HOME/terminfo"
export TERMINFO_DIRS="$XDG_DATA_HOME/terminfo:/usr/share/terminfo"
export GOPATH=$XDG_DATA_HOME/go
export GRADLE_USER_HOME=$XDG_DATA_HOME/gradle
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java
export NODE_REPL_HISTORY=$XDG_CACHE_HOME/node_repl_history
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export NPM_CONFIG_PREFIX="$XDG_DATA_HOME/npm-global"
export CARGO_HOME=$XDG_DATA_HOME/cargo
export RUSTUP_HOME=$XDG_DATA_HOME/rustup
export BUNDLE_USER_CONFIG=$XDG_CONFIG_HOME/bundle
export BUNDLE_USER_CACHE=$XDG_CACHE_HOME/bundle
export BUNDLE_USER_PLUGIN=$XDG_DATA_HOME/bundle
export GEM_HOME=$XDG_DATA_HOME/gem
export GEM_SPEC_CACHE=$XDG_CACHE_HOME/gem
export DOCKER_CONFIG=$XDG_CONFIG_HOME/docker
export GNUPGHOME=$XDG_DATA_HOME/gnupg
export JUPYTER_CONFIG_DIR=$XDG_CONFIG_HOME/jupyter
export IPYTHONDIR=$XDG_CONFIG_HOME/ipython
export XAUTHORITY=$XDG_RUNTIME_DIR/Xauthority
export XINITRC=$XDG_CONFIG_HOME/X11/xinitrc
export XSERVERRC=$XDG_CONFIG_HOME/X11/xserverrc
export WGETRC=$XDG_CONFIG_HOME/wget/wgetrc
export WEECHAT_HOME=$XDG_CONFIG_HOME/weechat
export RIPGREP_CONFIG_PATH=$XDG_CONFIG_HOME/ripgrep/config

# env
export PAGER="less -R"
export VISUAL=nvim
export EDITOR=nvim
export SVDIR=$HOME/.local/share/service
export _JAVA_AWT_WM_NONREPARENTING=1
export GPG_TTY="$(tty)"
export GOPROXY=direct
# fzf
export FZF_DEFAULT_OPTS="--multi --layout=reverse --inline-info
 --bind alt-j:preview-down,alt-k:preview-up
 --color fg:#D8DEE9,bg:#2E3440,hl:#A3BE8C,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C
 --color pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#4C566A,prompt:#81A1C1,marker:#EBCB8B"
# export FZF_DEFAULT_OPTS="--multi --layout=reverse --inline-info
#  --bind alt-j:preview-down,alt-k:preview-up
#  --color fg:-1,bg:-1,hl:#c678dd,fg+:#ffffff,bg+:#4b5263,hl+:#d858fe
#  --color info:#98c379,prompt:#61afef,pointer:#be5046,marker:#e5c07b,spinner:#61afef,header:#61afef"
export FZF_DEFAULT_COMMAND="fd --type f --follow --hidden --exclude .git"
export FZF_CTRL_T_OPTS="--no-height --preview-window 'left:60%' --preview '$HOME/.local/bin/garbage/preview {} 2>/dev/null'"
export FZF_CTRL_T_COMMAND="fd --follow --hidden --exclude .git"
export FZF_ALT_C_OPTS="--preview 'exa -1a --sort=type --color always --icons {} 2>/dev/null'"
export FZF_ALT_C_COMMAND="fd --type d --follow --hidden --exclude .git"
export FZF_TMUX=1
# .NET
export DOTNET_CLI_TELEMETRY_OPTOUT=1
# zoxide
export _ZO_DATA_DIR="$HOME/.local/share/zoxide"
export _ZO_ECHO=1
export _ZO_FZF_OPTS="$FZF_DEFAULT_OPTS --no-multi"
export _ZO_RESOLVE_SYMLINKS=1
# nodenv
export NODENV_ROOT="$HOME/.local/share/nodenv"
# pyenv
export PYENV_ROOT="$HOME/.local/share/pyenv"
# poetry
export POETRY_HOME="$HOME/.local/share/poetry"
# cht.sh
export CHTSH="$XDG_CONFIG_HOME/cht.sh"
# nnn
if command -v nnn >/dev/null
then
  export TERMINAL=alacritty # for the preview script
  export NNN_OPTS="HUedx"
  export NNN_PLUG='b:bulknew;c:fzcd;o:fzopen;z:fzz;d:gpgd;e:gpge;h:hexview;m:nmount;n:nuke;t:preview-tui;r:renamer;p:rsynccp;j:splitjoin;s:suedit;i:ringtone'
  export NNN_FIFO="/tmp/nnn.fifo"
  export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
  # export NNN_OPENER="$XDG_CONFIG_HOME/nnn/plugins/nuke"
  # export NNN_TRASH=1 # Use trash-cli
  export USE_SCOPE=1
  export NNN_COLORS='2345'
  export NNN_FCOLORS='c1e20402006006f7c6d6ab01'
  export NNN_BMS="h:~;C:~/Code;D:~/Downloads;P:~/Pictures;V:~/Videos;A:~/Media;U:~/Music;f:~/.config;l:~/.local/share;e:/etc;u:/usr/share;o:/opt;b:/boot;m:/media;M:/mnt;i:/run/media/$USER;v:/var;t:/tmp;d:/dev;s:/srv;r:/"
  export NNN_SSHFS="sshfs -o reconnect,idmap=user,follow_symlinks"
fi
# lf
if command -v lf >/dev/null
then
  export LF_ICONS="\
  tw=:\
  st=:\
  ow=:\
  dt=:\
  di=:\
  fi=:\
  ln=:\
  or=:\
  ex=:\
  *.7z=:\
  *.a=:\
  *.ace=:\
  *.alz=:\
  *.ai=:\
  *.apk=:\
  *.arc=:\
  *.arj=:\
  *.asm=:\
  *.asp=:\
  *.aup=:\
  *.avi=:\
  *.awk=:\
  *.bash=:\
  *.bat=:\
  *.bmp=:\
  *.bz=:\
  *.bz2=:\
  *.c=:\
  *.c++=:\
  *.cab=:\
  *.cbr=:\
  *.cbz=:\
  *.cc=:\
  *.class=:\
  *.clj=:\
  *.cljc=:\
  *.cljs=:\
  *.cmake=:\
  *.cmd=:\
  *.coffee=:\
  *.conf=:\
  *.cp=:\
  *.cpio=:\
  *.cpp=:\
  *.cs=:\
  *.csh=:\
  *.css=:\
  *.cue=:\
  *.cvs=:\
  *.cxx=:\
  *.d=:\
  *.dart=:\
  *.db=:\
  *.deb=:\
  *.diff=:\
  *.dll=:\
  *.doc=:\
  *.docx=:\
  *.dump=:\
  *.dwm=:\
  *.dz=:\
  *.edn=:\
  *.eex=:\
  *.efi=:\
  *.ejs=:\
  *.elf=:\
  *.elm=:\
  *.epub=:\
  *.ear=:\
  *.erl=:\
  *.esd=:\
  *.ex=:\
  *.exe=:\
  *.exs=:\
  *.f#=:\
  *.fifo=|:\
  *.fish=:\
  *.flac=:\
  *.flv=:\
  *.fs=:\
  *.fsi=:\
  *.fsscript=:\
  *.fsx=:\
  *.gem=:\
  *.gemspec=:\
  *.gif=:\
  *.go=:\
  *.gz=:\
  *.gzip=:\
  *.h=:\
  *.haml=:\
  *.hbs=:\
  *.hh=:\
  *.hpp=:\
  *.hrl=:\
  *.hs=:\
  *.htaccess=:\
  *.htm=:\
  *.html=:\
  *.htpasswd=:\
  *.hxx=:\
  *.ico=:\
  *.img=:\
  *.ini=:\
  *.iso=:\
  *.jar=:\
  *.java=:\
  *.jl=:\
  *.jpeg=:\
  *.jpg=:\
  *.js=:\
  *.json=:\
  *.jsx=:\
  *.key=:\
  *.ksh=:\
  *.leex=:\
  *.less=:\
  *.lha=:\
  *.lhs=:\
  *.log=:\
  *.lrz=:\
  *.lua=:\
  *.lz=:\
  *.lz4=:\
  *.lzh=:\
  *.lzma=:\
  *.lzo=:\
  *.m2v=:\
  *.m4a=:\
  *.m4v=:\
  *.markdown=:\
  *.md=:\
  *.mdx=:\
  *.mjpeg=:\
  *.mjpg=:\
  *.mjs=:\
  *.mkv=:\
  *.ml=λ:\
  *.mli=λ:\
  *.mng=:\
  *.mov=:\
  *.mp3=:\
  *.mp4=:\
  *.mp4v=:\
  *.mpeg=:\
  *.mpg=:\
  *.msi=:\
  *.mustache=:\
  *.nix=:\
  *.o=:\
  *.odt=:\
  *.ods=:\
  *.odp=:\
  *.ogg=:\
  *.pdf=:\
  *.php=:\
  *.pl=:\
  *.pm=:\
  *.png=:\
  *.pp=:\
  *.ppt=:\
  *.pptx=:\
  *.pro=:\
  *.ps1=:\
  *.psb=:\
  *.pub=:\
  *.py=:\
  *.pyc=:\
  *.pyd=:\
  *.pyo=:\
  *.r=ﳒ:\
  *.rake=:\
  *.rar=:\
  *.rb=:\
  *.rc=:\
  *.rlib=:\
  *.rmd=:\
  *.rom=:\
  *.rpm=:\
  *.rproj=鉶:\
  *.rs=:\
  *.rss=:\
  *.rtf=:\
  *.rz=:\
  *.s=:\
  *.sar=:\
  *.sass=:\
  *.scala=:\
  *.scss=:\
  *.sh=:\
  *.slim=:\
  *.sln=:\
  *.so=:\
  *.sql=:\
  *.styl=:\
  *.suo=:\
  *.svg=:\
  *.swift=:\
  *.swm=:\
  *.t=:\
  *.t7z=:\
  *.tar=:\
  *.taz=:\
  *.tbz=:\
  *.tbz2=:\
  *.tex=ﭨ:\
  *.tgz=:\
  *.tif=:\
  *.tiff=:\
  *.tlz=:\
  *.toml=:\
  *.ts=:\
  *.tsx=:\
  *.txz=:\
  *.tz=:\
  *.tzo=:\
  *.tzst=:\
  *.twig=:\
  *.vifm=:\
  *.vim=:\
  *.vimrc=:\
  *.vob=:\
  *.vue=﵂:\
  *.wav=:\
  *.war=:\
  *.webm=:\
  *.webmanifest=:\
  *.webp=:\
  *.wim=:\
  *.xbm=:\
  *.xbps=:\
  *.xcplayground=:\
  *.xhtml=:\
  *.xls=:\
  *.xlsx=:\
  *.xml=:\
  *.xpm=:\
  *.xul=:\
  *.xz=:\
  *.yaml=:\
  *.yml=:\
  *.z=:\
  *.zip=:\
  *.zoo=:\
  *.zsh=:\
  *.zip=:\
  *.zst=:\
  "
fi

# _________________
# ___    |__  /__(_)_____ ________
# __  /| |_  /__  /_  __ `/_  ___/
# _  ___ |  / _  / / /_/ /_(__  )
# /_/  |_/_/  /_/  \__,_/ /____/
#
# remap
alias x=extract
alias p="pulsemixer"
alias ls="exa -lF --icons --sort=type"
alias la="exa -laF --icons --sort=type"
alias lt="exa --tree"
alias cp="cp -vir"
alias mv="mv -vi"
alias rm="rm -vr"
alias mkdir="mkdir -pv"
alias no="grep -viP"
alias yarn="yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config"
alias tmux="TERM=screen-256color tmux"
alias startx="startx $HOME/.config/X11/xinitrc"
alias fehwpp="feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/*"
# colorizing
alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"
alias diff="diff --color=auto"
alias ncdu="ncdu --color=dark"
alias ip="ip -color=always"
# vim
alias v="nvim"
alias vi='fzf | xargs -r nvim -o'
# emacs
alias doom="$HOME/.config/emacs-config/doom/bin/doom"
alias e="emacs -nw"
alias em="emacsclient -c -a 'emacs'"
# rsync
alias rsyncdir="rsync -avP --delete-after"
# youtube-dl
alias yoump3="youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail"
alias youflac="youtube-dl --extract-audio --audio-format flac"
alias youlist="youtube-dl --yes-playlist"

# Lazy cd-ing
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# colorize man pages
man() {
    MANPATH=$HOME/.local/share/man:$MANPATH \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;36m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man $@
}

# nnn with cd on quit
if command -v nnn >/dev/null; then
  alias ncp="cat ${NNN_SEL:-${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection} | tr '\0' '\n'"

  n () {
      # Block nesting of nnn in subshells
      if [[ -n $NNNLVL ]] && [ "${NNNLVL:-0}" -ge 1 ]; then
          echo "nnn is already running"
          return
      fi

      # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
      # To cd on quit only on ^G, remove the "export" as in:
      #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
      # NOTE: NNN_TMPFILE is fixed, should not be modified
      export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

      # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
      # stty start undef
      # stty stop undef
      # stty lwrap undef
      # stty lnext undef

      nnn "$@"

      if [ -f "$NNN_TMPFILE" ]; then
              . "$NNN_TMPFILE"
              rm -f "$NNN_TMPFILE" > /dev/null
      fi
  }

  # nnn in dual pane mode (tmux)
  n2() {
    tmux new-session -d -s nnn -n nnn "nnn -Hc"
    tmux split-window -h "nnn -Hc"
    TERM=screen-256color tmux attach -t nnn:nnn
  }
fi

# typing speed testing
tt() {
  iconv -f UTF-8 -t ASCII "$1" | sed 's/--/-/g' \
      | sed '/^$/d' | sed 's/^[ \t]*//' | sed 's/\(^.\{1,80\}\).*/\1/' \
      > /tmp/tt.txt
  command tt /tmp/tt.txt
}

# ________    _______________
# __  ___/______  /__  /___(_)_____________ ________
# _____ \_  _ \  __/  __/_  /__  __ \_  __ `/_  ___/
# ____/ //  __/ /_ / /_ _  / _  / / /  /_/ /_(__  )
# /____/ \___/\__/ \__/ /_/  /_/ /_/_\__, / /____/
#                                   /____/
#
mkdir -p "$BASH_COMPLETION_USER_DIR/completions"

# github cli completion
# if command -v gh >/dev/null && [ ! -f "$BASH_COMPLETION_USER_DIR/completions/gh" ]; then
#   gh completion -s bash > "$BASH_COMPLETION_USER_DIR/completions/gh"
# fi

# kitten from kitty
if command -v kitty > /dev/null; then
  source <(kitty + complete setup bash)
fi

# Starship prompt
if command -v starship > /dev/null; then
  eval "$(starship init bash --print-full-init)"
fi

# direnv
if command -v direnv >/dev/null ; then
  eval "$(direnv hook bash)"
fi

# poetry
# if [ ! -d "$POETRY_HOME" ]; then
#   curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
# fi
# if [[ ! -f $BASH_COMPLETION_USER_DIR/completions/poetry ]]; then
#   poetry completions bash > "$BASH_COMPLETION_USER_DIR/completions/poetry"
# fi

# pyenv
# if [[ ! -d $PYENV_ROOT ]]; then
#   git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT
#   git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_ROOT/plugins/pyenv-virtualenv
#   git clone https://github.com/pyenv/pyenv-update.git $PYENV_ROOT/plugins/pyenv-update
# fi
# if shopt -q login_shell; then
#   eval "$(pyenv init --path)"
# fi
# eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"

# nodenv
# if [[ ! -d $NODENV_ROOT ]]; then
#   git clone https://github.com/nodenv/nodenv.git $NODENV_ROOT
#   mkdir -p $NODENV_ROOT/plugins
#   git clone https://github.com/nodenv/node-build.git $NODENV_ROOT/plugins/node-build
#   git clone https://github.com/nodenv/nodenv-env.git $NODENV_ROOT/plugins/nodenv-env
#   git clone https://github.com/nodenv/nodenv-man.git $NODENV_ROOT/plugins/nodenv-man
#   git clone https://github.com/nodenv/nodenv-aliases.git $NODENV_ROOT/plugins/nodenv-aliases
#   git clone https://github.com/nodenv/nodenv-each.git $NODENV_ROOT/plugins/nodenv-each
#   git clone https://github.com/nodenv/nodenv-update.git $NODENV_ROOT/plugins/nodenv-update
#   git clone https://github.com/nodenv/nodenv-vars.git $NODENV_ROOT/plugins/nodenv-vars
# fi
# eval "$(nodenv init -)"

# zoxide
if command -v zoxide >/dev/null; then
  eval "$(zoxide init bash)"
fi

# autostart gpg-agent at login
if ! pgrep -u "$USER" gpg-agent >/dev/null; then
  gpg-agent --daemon --enable-ssh-support >/dev/null
fi
if [[ -z "$SSH_AUTH_SOCK" ]]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
gpg-connect-agent updatestartuptty /bye >/dev/null

# vi mode
set -o vi
bind 'set show-mode-in-prompt on'
bind 'set vi-cmd-mode-string "\e[01;32mN "'
bind 'set vi-ins-mode-string "\e[01;32mI "'

# ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# Better default modes
umask 0027

# Shell options
shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s dotglob
shopt -s checkwinsize
shopt -s expand_aliases
shopt -s histappend

export PS2="\[\e[1;31m\]> "

# ______________              _____
# ___  __ \__  /___  ________ ___(_)______________
# __  /_/ /_  /_  / / /_  __ `/_  /__  __ \_  ___/
# _  ____/_  / / /_/ /_  /_/ /_  / _  / / /(__  )
# /_/     /_/  \__,_/ _\__, / /_/  /_/ /_//____/
#                     /____/
# forgit
if [ ! -f "$BASH_COMPLETION_USER_DIR/plugins/forgit.plugin.bash" ]; then
  curl -fLo $BASH_COMPLETION_USER_DIR/plugins/forgit.plugin.bash https://github.com/wfxr/forgit/raw/master/forgit.plugin.zsh
fi
source $BASH_COMPLETION_USER_DIR/plugins/forgit.plugin.sh

if [ -n "$XDG_SESSION_TYPE" ]; then
  if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export FORGIT_COPY_CMD="wl-copy"
  else
    if command -v xclip >/dev/null; then
      export FORGIT_COPY_CMD="xclip -selection clipboard"
    else
      export FORGIT_COPY_CMD="xsel -i -b"
    fi
  fi
fi

# If I don't want starship
# source $BASH_COMPLETION_USER_DIR/plugins/prompt.bash

# fzf
if [ ! -f "$BASH_COMPLETION_USER_DIR/plugins/fzf-completion.bash" ] || [ ! -f "$BASH_COMPLETION_USER_DIR/plugins/fzf-bindings.bash" ]; then
  curl -fLo $BASH_COMPLETION_USER_DIR/plugins/fzf-completion.bash https://github.com/junegunn/fzf/raw/master/shell/completion.bash
  curl -fLo $BASH_COMPLETION_USER_DIR/plugins/fzf-bindings.bash https://github.com/junegunn/fzf/raw/master/shell/key-bindings.bash
fi
source $BASH_COMPLETION_USER_DIR/plugins/fzf-completion.bash
source $BASH_COMPLETION_USER_DIR/plugins/fzf-bindings.bash
export FZF_COMPLETION_OPTS='-x'

# Use fd instead of find for fzf_completion
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

# Some more completions with fzf
_fzf_setup_completion path ag kubectl
_fzf_setup_completion dir tree

# ^X^R
bind "$(bind -s | grep '^"\\C-r"' | sed 's/"/"\\C-x/' | sed 's/"$/\\C-m"/')"

$HOME/.local/bin/bunny
