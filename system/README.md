### System

This directory contains some system configuration templates.

### Networking notes

To setup networking without any network managers, dhcp client, etc.

```
# Add an address
ip address add 192.168.0.120/24 broadcast + dev eth0

# Add a route
ip route add default via 192.168.0.1 dev eth0
#or s|default|192.168.0.0/24|

# Set up DNS in /etc/resolv.conf
# nameserver 192.168.0.1
# nameserver 8.8.8.8
```
