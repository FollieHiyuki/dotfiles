#!/bin/sh

echo "Shell (bash/zsh/fish): " | tr -d '\n'
read -r shell
if [ -z "${shell}" ]
then exit
fi

echo "Display server (wayland/x11): " | tr -d '\n'
read -r server
if [ -z "${server}" ]
then exit
fi

echo "
~~~ Installing shell config ~~~
"
case ${shell} in
  bash)
    cp -rfv ./home/.bashrc-bloated    ~/.bashrc
    cp -rfv ./home/.bash_profile      ~/.bash_profile
    cp -rfv ./home/.local/share/bash/ ~/.local/share/bash/
    ;;
  zsh)
    # Need to add `export ZDOTDIR=$HOME/.config/zsh` to /etc/zsh/zshenv
    cp -rfv ./home/.bashrc           ~/.bashrc
    cp -rfv ./home/.config/zsh/      ~/.config/zsh/
    cp -rfv ./home/.local/share/zsh/ ~/.local/share/zsh/
    ;;
  fish)
    cp -rfv ./home/.bashrc       ~/.bashrc
    cp -rfv ./home/.config/fish/ ~/.config/fish/
    ;;
  *)
    exit ;;
esac

echo "
~~~ Installing ${server} specified config ~~~
"
case ${server} in
  wayland)
    cp -rfv ./home/.config/foot/                   ~/.config/foot/
    cp -rfv ./home/.config/hikari/                 ~/.config/hikari/
    # Dunst is Wayland native too, since 1.6.0
    if command -v dunst >/dev/null
    then
      cp -rfv ./home/.config/dunst/                ~/.config/dunst/
    else
      cp -rfv ./home/.config/mako/                 ~/.config/mako/
    fi
    cp -rfv ./home/.config/nwg-launchers/          ~/.config/nwg-launchers/
    cp -rfv ./home/.config/river/                  ~/.config/river/
    cp -rfv ./home/.config/sway/                   ~/.config/sway/
    cp -rfv ./home/.config/swaylock/               ~/.config/swaylock/
    cp -rfv ./home/.config/waybar/                 ~/.config/waybar/
    cp -rfv ./home/.config/wofi/                   ~/.config/wofi/
    cp -rfv ./home/.config/xdg-desktop-portal-wlr/ ~/.config/xdg-desktop-portal-wlr/
    # qimgv is preferable
    # cp -rfv ./home/.config/imv/                    ~/.config/imv/
    # cp -rfv ./home/.config/pqivrc                  ~/.config/pqivrc
    cp -rfv ./home/.config/wayfire.ini             ~/.config/wayfire.ini
    ;;
  x11)
    cp -rfv ./home/.config/bsp-layout/    ~/.config/bsp-layout/
    cp -rfv ./home/.config/bspwm/         ~/.config/bspwm/
    cp -rfv ./home/.config/dunst/         ~/.config/dunst/
    cp -rfv ./home/.config/gtk-3.0/       ~/.config/gtk-3.0/
    cp -rfv ./home/.config/kitty/         ~/.config/kitty/
    cp -rfv ./home/.config/polybar/       ~/.config/polybar/
    cp -rfv ./home/.config/rofi/          ~/.config/rofi/
    cp -rfv ./home/.config/spectrwm/      ~/.config/spectrwm/
    cp -rfv ./home/.config/sxhkd/         ~/.config/sxhkd/
    cp -rfv ./home/.config/X11/           ~/.config/X11/
    cp -rfv ./home/.config/greenclip.toml ~/.config/greenclip.toml
    cp -rfv ./home/.config/picom.conf     ~/.config/picom.conf
    ;;
  *)
    exit ;;
esac

echo "
~~~ Installing standard config ~~~
"
# Assests
mkdir -pv ~/Pictures
cp -rfv ./home/Pictures/Animated/   ~/Pictures/Animated/
cp -rfv ./home/Pictures/Wallpapers/ ~/Pictures/Wallpapers/
cp -rfv ./home/.local/share/fonts/  ~/.local/share/fonts/
# Scripts
cp -rfv ./home/.local/bin/ ~/.local/bin/
# The bloated dotfiles itself
cp -rfv ./home/.mbsyncrc                 ~/.mbsyncrc
cp -rfv ./home/.rtorrent.rc              ~/.rtorrent.rc
cp -rfv ./home/.config/alacritty/        ~/.config/alacritty/
cp -rfv ./home/.config/amfora/           ~/.config/amfora/
cp -rfv ./home/.config/anime-downloader/ ~/.config/anime-downloader/
cp -rfv ./home/.config/aria2/            ~/.config/aria2/
cp -rfv ./home/.config/bat/              ~/.config/bat/
cp -rfv ./home/.config/bottom/           ~/.config/bottom/
cp -rfv ./home/.config/cava/             ~/.config/cava/
cp -rfv ./home/.config/chemacs/          ~/.config/chemacs/
cp -rfv ./home/.config/cht.sh/           ~/.config/cht.sh/
cp -rfv ./home/.config/cointop/          ~/.config/cointop/
cp -rfv ./home/.config/doom/             ~/.config/doom/
cp -rfv ./home/.config/Element/          ~/.config/Element/
cp -rfv ./home/.config/fontconfig/       ~/.config/fontconfig/
cp -rfv ./home/.config/gallery-dl/       ~/.config/gallery-dl/
cp -rfv ./home/.config/GIMP/             ~/.config/GIMP/
cp -rfv ./home/.config/git/              ~/.config/git/
cp -rfv ./home/.config/glow/             ~/.config/glow/
# cp -rfv ./home/.config/lazygit/          ~/.config/lazygit/
# cp -rfv ./home/.config/lite-xl/          ~/.config/lite-xl/
cp -rfv ./home/.config/mpd/              ~/.config/mpd/
cp -rfv ./home/.config/mpDris2/          ~/.config/mpDris2/
cp -rfv ./home/.config/mpv/              ~/.config/mpv/
cp -rfv ./home/.config/ncmpcpp/          ~/.config/ncmpcpp/
# cp -rfv ./home/.config/neofetch/         ~/.config/neofetch/
cp -rfv ./home/.config/newsboat/         ~/.config/newsboat/
cp -rfv ./home/.config/nnn/              ~/.config/nnn/
cp -rfv ./home/.config/npm/              ~/.config/npm/
cp -rfv ./home/.config/nvim/             ~/.config/nvim/
cp -rfv ./home/.config/pipe-viewer/      ~/.config/pipe-viewer/
cp -rfv ./home/.config/QtProject/        ~/.config/QtProject/
# cp -rfv ./home/.config/qutebrowser/      ~/.config/qutebrowser/
cp -rfv ./home/.config/ranger/           ~/.config/ranger/
cp -rfv ./home/.config/ripgrep/          ~/.config/ripgrep/
# cp -rfv ./home/.config/tg/               ~/.config/tg/
cp -rfv ./home/.config/tmux/             ~/.config/tmux/
cp -rfv ./home/.config/translate-shell/  ~/.config/translate-shell/
# cp -rfv ./home/.config/tridactyl/        ~/.config/tridactyl/
cp -rfv ./home/.config/vifm/             ~/.config/vifm/
cp -rfv ./home/.config/weechat/          ~/.config/weechat/
cp -rfv ./home/.config/wget/             ~/.config/wget/
cp -rfv ./home/.config/youtube-dl/       ~/.config/youtube-dl/
cp -rfv ./home/.config/ytmdl/            ~/.config/ytmdl/
cp -rfv ./home/.config/zathura/          ~/.config/zathura/
# cp -rfv ./home/.config/zellij/           ~/.config/zellij/
cp -rfv ./home/.config/mimeapps.list     ~/.config/mimeapps.list
cp -rfv ./home/.config/pulsemixer.cfg    ~/.config/pulsemixer.cfg
cp -rfv ./home/.config/starship.toml     ~/.config/starship.toml
# gpg
cp -rfv ./home/.local/share/gnupg/       ~/.local/share/gnupg/

echo "
~~~ Cloning submodules ~~~
"
[ -d ~/.config/emacs ] || git clone https://github.com/plexus/chemacs2 ~/.config/emacs
[ -d ~/.config/emacs-config/doom ] || git clone https://github.com/hlissner/doom-emacs ~/.config/emacs-config/doom
[ -d ~/.config/tmux/plugins/tpm ] || git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
[ -d ~/.config/ranger/plugins/ranger_devicons ] || git clone https://github.com/FollieHiyuki/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
[ -d ~/.config/ranger/plugins/ranger-zoxide ] || git clone https://github.com/jchook/ranger-zoxide ~/.config/ranger/plugins/ranger-zoxide

echo "
~~~ Post deployment ~~~
"
# Needed data dirs
mkdir -pv ~/.local/share/bash
mkdir -pv ~/.local/share/mpd/playlists
mkdir -pv ~/.local/share/gallery-dl

# tridactyl needs `tridactylrc` to work
# touch ~/.config/tridactyl/tridactylrc

# Strict permissions
chmod -v 700 ~/.local/share/gnupg
chmod -v 600 ~/.local/share/gnupg/*
chmod -v 600 ~/.config/aria2/aria2.conf

# chsh -s /usr/bin/${shell} ${USER}

echo "
~~~ Finished ~~~"
