<p align="center">
    <img src="assets/dotfiles2.png" height="200px" alt="dotfiles_icon"/>
</p>

### :warning: Deprecation

This repository is deprecated since lots of stuff within it I don't regularly use, and they are poorly maintained.

New changes for user config will be pushed to [dotfiles-ansible](/FollieHiyuki/dotfiles-ansible)

For updated system config, look at [sysconfig](/FollieHiyuki/sysconfig)

### :question: What can you find here

<img src="assets/AnimuThinku.png" width="121px" align="left" alt="AnimuThinku"></a>

  **My personal dotfiles, which:**

  => is bloated (I use a lot of programs, sometimes just to try out)  
  => yet clean (follow XDG base directory specification)  
  => for [bspwm](https://github.com/baskerville/bspwm), [spectrwm](https://github.com/conformal/spectrwm), ~~[exwm](https://github.com/ch11ng/exwm)~~ (deprecated after commit 383de337f4), [sway](https://github.com/swaywm/sway), [wayfire](https://github.com/WayfireWM/wayfire), [river](https://github.com/ifreund/river), [hikari](https://hikari.acmelabs.space/)  
  => and features [Nord](https://github.com/arcticicestudio/nord) + [OneDark](https://github.com/joshdick/onedark.vim) colorschemes

![sway](assets/sway.png)

### :clipboard: TODO

- [ ] ~~Additional palette: [Material](https://material-theme.site/) / [Ayu](https://github.com/ayu-theme/ayu-colors)~~
- [ ] ~~[dotdrop](https://github.com/deadc0de6/dotdrop)~~ / ~~Org Babel~~ / Ansible
- [x] Migrate zsh to [zinit](https://github.com/zdharma/zinit)
- [x] Wayland compositors
- [x] New Neovim config in Lua
    - [ ] OneDark highlight groups
- [ ] ~~[ion](https://github.com/redox-os/ion)~~ / [oksh](https://github.com/ibara/oksh) / [nushell](https://www.nushell.sh/) / [oil](https://www.oilshell.org/) / [xonsh](https://xon.sh/) / ~~[elvish](https://elv.sh/)~~ / [oh](https://github.com/michaelmacinnis/oh)
- [ ] NixOS / Guix / Gentoo / FreeBSD
- [ ] Independent ~/.emacs.d
- [ ] [kakoune](https://github.com/mawww/kakoune) / [lite-xl](https://github.com/franko/lite-xl)

### :star2: Credits

- [@glepnir](https://github.com/glepnir/nvim)'s Neovim config, also [NvChad](https://github.com/NvChad/NvChad) and [nord.nvim](https://github.com/shaunsingh/nord.nvim)
- [@novakane](https://git.sr.ht/~novakane/) for git aliases
- [@hlissner](https://github.com/hlissner)'s zsh config
- [@daviwil](https://github.com/daviwil)'s/[@tecosaur](https://tecosaur.github.io/emacs-config/config.html)'s Emacs configurations
- [@begs](https://git.sr.ht/~begs/dotfiles)'s Waybar config
- [@nathanielevan](https://github.com/nathanielevan/dotfiles)'s weechat config
- [@iohanaan](https://gitlab.com/iohanaan/qtcreator-onedark)'s qtcreator theme
- [some fzf scripts](https://github.com/DanielFGray/fzf-scripts)
- [sysctl.conf template](https://github.com/k4yt3x/sysctl). And also check out [this article](https://madaidans-insecurities.github.io/guides/linux-hardening.html)
- Artists for **kawaii** wallpapers: [@rimuu](https://rimuu.com/), [@hiten](https://www.pixiv.net/users/490219/artworks), [@Tiv](https://www.pixiv.net/en/users/35081), [@mery](https://www.pixiv.net/en/users/2750098), [@Mashima_saki](https://www.pixiv.net/en/users/18403608), [@Yuuki_Tatsuya](https://www.pixiv.net/en/users/27691), [@Bison倉鼠](https://www.pixiv.net/en/users/333556/artworks), [@Dana](https://twitter.com/hapong07), [@gomzi](https://twitter.com/gcmzi), [@Rella](https://twitter.com/Rellakinoko), [@dnwls3010](https://twitter.com/dnwls3010), [@Shigure_Ui](https://www.pixiv.net/en/users/431873), [@QuAn_](https://www.pixiv.net/en/users/6657532/artworks), [@杉87](https://twitter.com/k_su_keke1121), [@fuzichoco](https://twitter.com/fuzichoco), [@Astero](https://twitter.com/asteroid_ill), [@shin556](https://www.pixiv.net/en/users/642762), [@kaynimatic](https://twitter.com/kaynimatic)

### :label: License

MIT
